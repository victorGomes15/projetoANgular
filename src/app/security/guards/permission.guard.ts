import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/';
import { AlertService } from '../../components/alert/_services';
import { SessionService } from '../../components/common/_service/session.service';
import { ConnectContService } from '../../components/connectcont/connectcont.services';
import { AcessoNegadoService } from '../acessoNegado/acessoNegado.service';
import { AcessoNegadoComponent } from '../acessoNegado/acessoNegado.component';
import { BasicLayoutComponent } from '../../components/common/layouts/basicLayout.component';
import { CleanLayoutComponent } from '../../components/common/layouts/cleanLayout.component';

@Injectable()
export class PermissionGuard implements CanActivate {

  defaltMsg = {
    tipos: [ 'acessoNegado' ],
    mensagem: { resolve: (vars) => `${vars.usuarioLogado}, você não possui permissão de acesso a essa rotina` }
  };

  constructor(
    private router: Router,
    private _service: ConnectContService,
    private alert: AlertService,
    private acessoNegadoService: AcessoNegadoService
  ) {}

   canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {
    const roles = route.data['roles'] as Array<string>;
    this.acessoNegadoService.mensagens = ( route.data['msg'] || [ this.defaltMsg ] )
        .filter( item => item.tipos.includes( 'acessoNegado' ) ).map( item => item.mensagem );
    const promisse = this._service.hasAnyRoles(roles).toPromise();
    promisse.then(
      data => {
        if (!data) {
          // Utilizado replaceUrl para resolver problemas de voltar para tela de login ao clicar no botao de voltar do navegador (historico)
          let replace = false;
          if (this.isAnyComponentClean(route)) {
            replace = true;
          }
          this.router.navigate(['negado'], { queryParams: route.queryParams, queryParamsHandling: 'merge', replaceUrl: replace });
        }
        return Promise.resolve(true);
      }
    ).catch(err => {
        this.alert.error( 'Erro ao carregar permissões. Atualize a página e tente novamente em instantes.' );
        // this.alert.error( this._errorHandlerService.getErrorMsg(
        //     error,
        //     'Erro ao carregar permissões. Atualize a página e tente novamente em instantes.'
        //   ) );
        this.router.navigate(['/home'], { queryParams: route.queryParams, queryParamsHandling: 'merge' });
        return Promise.resolve(false);
    });
    return promisse;
  }

  /**
   * Metodo  utilizado para resolver problemas no embarcado
   *
   * @param route
   */

  private isAnyComponentClean(route): boolean {
    let parentAtual = route.parent;
    let isClean = false;
    while (parentAtual) {
      if (parentAtual.component === CleanLayoutComponent || parentAtual === CleanLayoutComponent) {
        isClean = true;
        break;
      }
      parentAtual = parentAtual.parent;
    }
    return isClean;
  }

}
