//https://angular.io/guide/styleguide#constants
import { environment } from '../../../environments/environment';

//auth
export const clientId = '3';
export const clientId_8 = '8';
export const accessToken = 'access_token';
export const refreshToken = 'refresh_token';
export const grantTypePassword = 'password';
export const grantTypeClientCredentials = 'client_credentials';
export const clientSecret_8 = 'bdbd3b7e-9060-42ac-9e16-72b96caca840';
export const username = 'session_login';

//cookie
export const cookieRefreshToken = 'refresh_token';

//url
export const urlToken: string = environment.urlSSO + '/oauth/token';