import { HttpClient, HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'app/components/util/rxjs-extensions.ts';
import { Cookie } from 'ng2-cookies';
import { Observable, Subject } from 'rxjs/';
import { catchError, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import * as oauth2 from '../oauth2/oauth2.constants';
import { OAuth2Service } from './oauth2.service';

// Baseado neste interceptor
// https://github.com/IntertechInc/http-interceptor-refresh-token/blob/master/src/app/request-interceptor.service.ts
@Injectable()
export class OAuth2Interceptor implements HttpInterceptor {

  private processing: Observable<any>;
  private subject;

  /*
  algumas paginas ao iniciar realizam diversas requisicoes
  para diversos servicos do sso,
  da forma que estava a primeira requisicao para sso
  com 401 ia para processando
  e as demais lancariam erro para a pagina
*/

  constructor(
    private oauth2Service: OAuth2Service, private httpClient: HttpClient, private router: Router,
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.startsWith(environment.urlSSOPublico) || req.url.startsWith(environment.urlSSO)) {

      return next.handle(req).pipe(
        catchError(error => {
          if (error instanceof HttpErrorResponse) {
            const responseError: HttpErrorResponse = (<HttpErrorResponse>error);
            if (responseError.status === 401 && responseError.error.error === 'invalid_token') {
              if (responseError.url === environment.urlSSO + '/oauth/token') {
                return this.handleSessionExpired();
              }
              return this.handle401(req, next);
            }
            if (this.check400(responseError, req)) {
              return this.handleSessionExpired();
            }
          }
          return Observable.throw(error); // Se não for necessário interceptar o erro, lançar o mesmo erro.
        })
      );
    } else {
      return next.handle(req);
    }
  }

  private check400(responseError: HttpErrorResponse, req: HttpRequest<any>) {
    return responseError.status === 400
      && responseError.error.error === 'invalid_grant'
      && req.url === oauth2.urlToken
      && responseError.error.error_description.startsWith('Invalid refresh token: ');
  }

  handle401(req: HttpRequest<any>, next: HttpHandler) {
    if (Cookie.check(oauth2.cookieRefreshToken)
      || !this.processing || (req.url !== oauth2.urlToken)) {
      if (!this.processing) {
        this.subject = new Subject();
        this.processing = new Observable((observer) => this.subject.subscribe(observer));
        const params: HttpParams = this.oauth2Service.getRefreshTokenParams();
        this.oauth2Service.obtainToken(params).subscribe(this.subject);
      }
      return this.processing
        .pipe(
          switchMap((data: any) => {
            if (data) {
              this.processing = undefined;
              return next.handle(this.updateToken(req, data));
            } else {
              return this.handleSessionExpired();
            }
          }),
          catchError(error => Observable.throw(error))
        );
    } else {
      return this.handleSessionExpired();
    }
  }

  handleSessionExpired() {
    if (this.processing) {
      const aux = this.subject;
      this.processing = undefined;
      aux.next();
    }
    this.router.navigate(['/login', 'session'], { queryParamsHandling: 'merge'});
    return Observable.empty();
  }

  updateToken(req: HttpRequest<any>, data: any): HttpRequest<any> {
    let newHeaders = new HttpHeaders();
    req.headers.keys().forEach(element => {
      newHeaders = newHeaders.set(element, req.headers.get(element));
    });
    newHeaders = newHeaders.set('Authorization', 'Bearer ' + data.access_token);
    const cloned = req.clone({ headers: newHeaders });
    return cloned;
  }

}
