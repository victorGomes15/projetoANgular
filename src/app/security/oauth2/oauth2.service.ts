import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Cookie } from 'ng2-cookies';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Token } from '../_models/token.model';
import * as oauth2 from '../oauth2/oauth2.constants';

@Injectable()
export class OAuth2Service {

    constructor(
        private _router: Router, private _httpClient: HttpClient
    ) { }

    saveTokenDataInCookie(access_token, refresh_token, expires_in) {
        const expireDate = new Date(new Date().getTime() + (1000 * expires_in));
        Cookie.set(oauth2.accessToken, access_token, expireDate, '/');
        Cookie.set(oauth2.refreshToken, refresh_token, undefined, '/');
    }

    saveTokenDataInCookieContadorOnline(access_token, refresh_token, expires_in, session_login) {
        const expireDate = new Date(expires_in);
        const lastToken = Cookie.get(oauth2.accessToken);
        if (lastToken !== access_token) {
            localStorage.removeItem('usuario');
        }

        Cookie.set(oauth2.accessToken, access_token, expireDate, '/');
        Cookie.set(oauth2.refreshToken, refresh_token, undefined, '/');
        sessionStorage.setItem('session_login', session_login);
    }

    saveTokenInCookie(token) {
        this.saveTokenDataInCookie(token.access_token, token.refresh_token, token.expires_in);
    }

    getTokenParams(): HttpParams {
        return new HttpParams()
            .append('client_id', environment.clientId)
            .append('client_secret', environment.clientSecret);
    }

    getRefreshTokenParams(): HttpParams {
        return this.getTokenParams()
            .append('grant_type', oauth2.refreshToken)
            .append('refresh_token', Cookie.get(oauth2.refreshToken));
    }

    private getTokenHeaders(): any {
        return { headers: new HttpHeaders({ Authorization: 'Basic ' + btoa(environment.clientId + ':' + environment.clientSecret), 'Content-type': 'application/x-www-form-urlencoded; charset=utf-8' }) };
    }

    obtainToken(params: HttpParams) {
        const options = this.getTokenHeaders();
        const retorno = this._httpClient.post<Token>(environment.urlSSO + '/oauth/token', params.toString(), options).publish();
        retorno.subscribe(data => this.saveTokenInCookie(data), err => Observable.throw(err));
        retorno.connect();
        return retorno;
    }

    private getHeaderWithAuthorization(authorizationToken: string): HttpHeaders {
        let headers = new HttpHeaders();
        headers = headers.append('Accept', 'application/json');
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', 'Bearer ' + authorizationToken);
        return headers;
    }

    validarClientId(authorizationToken: string, clienteId: string): Observable<any> {
        return this._httpClient.get(
            environment.urlSSO + '/guards/clients/' + clienteId,
            { headers: this.getHeaderWithAuthorization(authorizationToken) }
        );
    }
}
