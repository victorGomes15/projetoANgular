import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AcessoNegadoService } from './acessoNegado.service';
import { environment } from '../../../environments/environment.prod';
import { ConnectContService } from '../../components/connectcont/connectcont.services';

@Component({
  moduleId: module.id, // Obrigatorio se for trabalhar com templateUrl ou para referenciar qualquer arquivo externo
  selector: 'app-acessoNegado',
  templateUrl: './acessoNegado.component.html',
  styleUrls: [ './acessoNegado.component.scss' ],
})
export class AcessoNegadoComponent implements OnInit, AfterViewInit {

  block_img = `${environment.srvImgAcessoNegado}/img/site/block.png`;

  constructor(
    public connectContService: ConnectContService,
    public service: AcessoNegadoService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    const service = this.service;
    this.connectContService.getUsuario().subscribe(
      data => {
        this.service.mensagens.forEach(mensagem => {
          if (mensagem.resolve) {
            mensagem.data = Object.assign( mensagem.data || {}, { usuarioLogado: data.pessoaFisica.nome } );
            mensagem.resolved = mensagem.resolve( mensagem.data );
          }
        });
      }
    );
  }

  ngAfterViewInit (): void {
  }

}
