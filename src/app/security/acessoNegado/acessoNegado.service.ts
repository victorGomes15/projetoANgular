import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

declare var jQuery: any;

@Injectable()
export class AcessoNegadoService {

    public mensagens: { data?, resolved?, resolve? }[] = [];

    constructor(
        private router
            : Router,
    ) { }

    acessoNegado(... mensagens: { data?, resolved?, resolve? }[]) {
        this.mensagens = mensagens;
        this.router.navigate(['/negado'], { queryParamsHandling: 'merge' });
    }

}
