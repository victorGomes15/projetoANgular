import { AfterViewInit, Component, HostListener, OnInit, ViewEncapsulation, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { ActivationEnd, ActivationStart, Router, NavigationStart } from '@angular/router';
import { UUID } from 'angular2-uuid';
import { ConnectableObservable, Observable, Subject } from 'rxjs/';
import { AlertService } from '../../alert/_services';
import { SessionService } from '../_service/session.service';
import { IAlertConfigParameter } from '../../alert/_models';

declare var jQuery: any;

@Component({
  selector: 'app-clean',
  templateUrl: 'cleanLayout.template.html',
  styles: [
    `
      body { background-color: transparent !important; }
      .frameTop { position: absolute; right: 0; }
      #wrapper { position: absolute; }
      #ratioDiv { position: absolute; top: 0; left: 0; width: 100%; min-height: 100%; z-index: -1; }
    `
  ],
  encapsulation: ViewEncapsulation.None,
})
export class CleanLayoutComponent implements OnInit, AfterViewInit {

  height = 1;
  observable: ConnectableObservable<boolean> = Observable.empty<boolean>().publish();
  afterFrameSet = new EventEmitter<any>();

  constructor(
    public sessionService: SessionService,
    private _router: Router,
    private alert: AlertService,
  ) {
    _router.events.subscribe(event => {
      if (event instanceof ActivationStart) {
        if (event.snapshot.component === CleanLayoutComponent) {
          this.sessionService.reload();
          const subject = new Subject<boolean>();
          this.observable = subject.asObservable().publishReplay(1);
          this.observable.connect();
          subject.next(true);
          subject.complete();
        }
      }
      if (event instanceof ActivationEnd) {
        if (event.snapshot.component === CleanLayoutComponent) {
          this.sendResize(1);
        }
      }
    });
  }

  @ViewChild('ratioDiv') ratioDiv: ElementRef;

  ngOnInit() {
    jQuery('.sk-spinner').css('top', jQuery('.sk-spinner').css('top'));
    this.sessionService.register('layout', this);
  }

  ngAfterViewInit() {
    this.sendEvent('ajustSize');
    setInterval(() => this.checkResize('time'), 90);
  }

  sendResize(height) {
    this.height = height;
    this.sendEvent('resize', {
      'frame_size': height,
      'frame_scroll': jQuery('#ratioDiv')[0].scrollHeight,
    });
  }

  sendEvent(event, data = {}) {
    if (window.top && (window.top !== window)) {
      const id = UUID.UUID();
      window.top.postMessage({
        'message_hash': id,
        'frame_data': data,
        'evento': event
      }, '*');
    }
  }

  sendRedirect(data = {}) {
    data['conversationData'] = { alerts: this.alert.mensagens };
    this.sendEvent('redirect', data);
  }

  checkResize(origin) {
    const $wrapper = jQuery('#wrapper');
    const height = $wrapper[0].scrollHeight + $wrapper.offset().top;
    const ratio = jQuery(this.ratioDiv.nativeElement)[0];
    if (window.top && window.top !== window && ratio && height !== ratio.scrollHeight) {
      this.sendResize(height);
    }
  }

  @HostListener('window:message', ['$event'])
  onMessage(evento) {
    if (evento && evento.data) {
      if (evento.data.top !== undefined && evento.data.left !== undefined) {
        jQuery('.frameTop').offset({ top: evento.data.top, left: evento.data.left });
        this.afterFrameSet.next();
        this.afterFrameSet.complete();
      } else if (evento.data.conversationData) {
        const subscription = this._router.events.subscribe(event => {
          if (event instanceof NavigationStart) {
            this.hide(this.alert);
            evento.data.conversationData.alerts.forEach( alert => this.alert.remove(alert) );
            subscription.unsubscribe();
          }
        });
        this.alert.mensagens = evento.data.conversationData.alerts;
      }
    }
  }

  hide(alertConfig: IAlertConfigParameter = { alertClass: 'alertas' }) {
    jQuery('.' + alertConfig.alertClass).hide();
  }
}
