import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {BsDropdownModule} from 'ngx-bootstrap';

import {BasicLayoutComponent} from "./basicLayout.component";
import {BlankLayoutComponent} from "./blankLayout.component";
import {TopNavigationLayoutComponent} from "./topNavigationLayout.component";

import {NavigationComponent} from "./../navigation/navigation.component";
import {FooterComponent} from "./../footer/footer.component";
import {TopNavbarComponent} from "./../topnavbar/topnavbar.component";
import {TopNavigationNavbarComponent} from "./../topnavbar/topnavigationnavbar.component";
import { DataTableComponent } from "../../data-table/data-table.component";
import { CleanLayoutComponent } from "./cleanLayout.component";
import { SlideComponent } from "../slide/slide.component";
import { CommonModule } from "@angular/common";
import { AppModule } from "../../../app.module";
import { ContadorOnlineModule } from '../../../projeto/projeto.module';


@NgModule({
  declarations: [
    FooterComponent,
    BasicLayoutComponent,
    BlankLayoutComponent,
    CleanLayoutComponent,
    NavigationComponent,
    TopNavigationLayoutComponent,
    TopNavbarComponent,
    TopNavigationNavbarComponent,
    SlideComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ContadorOnlineModule,
    BsDropdownModule.forRoot()
  ],
  exports: [
    FooterComponent,
    BasicLayoutComponent,
    BlankLayoutComponent,
    CleanLayoutComponent,
    NavigationComponent,
    TopNavigationLayoutComponent,
    TopNavbarComponent,
    TopNavigationNavbarComponent,
    SlideComponent,
  ],
})

export class LayoutsModule {}
