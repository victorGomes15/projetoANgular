import { Component, AfterViewInit, OnInit, HostListener } from '@angular/core';
import { detectBody } from '../../../app.helpers';
import { Router, ActivationStart, ActivationEnd } from '@angular/router';
import { SessionService } from '../_service/session.service';
import { SlideComponent } from '../slide/slide.component';

declare var jQuery: any;

@Component({
  selector: 'basic',
  templateUrl: 'basicLayout.template.html',
  styles: [
    `
      .theme-config {
        overflow: unset;
      }
    `
  ],
})
export class BasicLayoutComponent implements OnInit, AfterViewInit {

  constructor(
    public sessionService: SessionService,
    private _router: Router,
  ) {
    _router.events.subscribe(event => {
      if (event instanceof ActivationStart) {
        if (event.snapshot.component === BasicLayoutComponent) {
          this.sessionService.reload();
        }
      }
    });
  }

  ngAfterViewInit() {
    this.sessionService.waitOne('slider1').subscribe( (slider: SlideComponent) => slider.refreshTop(140) );
  }

  public ngOnInit(): any {
    detectBody();
    this.sessionService.register('layout', this);
  }

  sendEvent(event, data = {}) {
  }

  @HostListener('window:resize', ['$event'])
  public onResize() {
    detectBody();
  }

}
