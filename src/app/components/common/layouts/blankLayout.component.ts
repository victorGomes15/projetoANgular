import { Component, HostListener } from '@angular/core';

declare var jQuery:any;

@Component({
  selector: 'blank',
  templateUrl: 'blankLayout.template.html',
  styles: [`.mb10 { margin-bottom: 10px; }`]
})
export class BlankLayoutComponent {

  ngAfterViewInit() {
    jQuery('body').addClass('gray-bg');
  }

  ngOnDestroy() {
    jQuery('body').removeClass('gray-bg');
  }
}
