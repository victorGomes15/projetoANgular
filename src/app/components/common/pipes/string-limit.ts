import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'stringLimit'})
export class StringLimitPipe implements PipeTransform {
    transform(value: string, quantidade?: number) {
        if (quantidade) {
            if (value && value.length > quantidade) {
                return value.substring(0, quantidade).concat('...');
            }
        }

        if (value && value.length > 40) {
            return value.substring(0, 40).concat('...');
        }
        return value;
    }
}
