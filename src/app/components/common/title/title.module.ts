import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SiteTitleComponent } from './title.component';
import { ContadorOnlineTitleComponent } from './contadoronline/contador-online-title/contador-online-title.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [
    SiteTitleComponent,
    ContadorOnlineTitleComponent,
  ],
  exports: [
    SiteTitleComponent,
    ContadorOnlineTitleComponent,
  ],
})

export class TitleModule { }