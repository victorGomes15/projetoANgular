import { Component, Input } from '@angular/core';

@Component({
  selector: 'site-title',
  templateUrl: 'title.template.html',
})
export class SiteTitleComponent {

  @Input() titulo: string;
  @Input() breadcrumbs: any[];

  siteBreadcrumbs = [ 'Contador Online', 'Minha Página' ];

  ngOnInit() {
    this.siteBreadcrumbs = this.siteBreadcrumbs.concat(this.breadcrumbs);
  }

}
