import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContadorOnlineTitleComponent } from './contador-online-title.component';

describe('ContadorOnlineTitleComponent', () => {
  let component: ContadorOnlineTitleComponent;
  let fixture: ComponentFixture<ContadorOnlineTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContadorOnlineTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContadorOnlineTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
