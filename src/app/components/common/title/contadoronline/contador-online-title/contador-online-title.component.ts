import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'contador-online-title',
  templateUrl: './contador-online-title.component.html',
  styleUrls: ['./contador-online-title.component.scss']
})
export class ContadorOnlineTitleComponent implements OnInit {

  @Input() titulo: string;
  @Input() breadcrumbs: any[];
  @Input() nomeBotao: string;
  @Input() esconderBotao: boolean;
  @Input() icone: string;
  @Input() btnClass = 'btn btn-primary';
  @Output() clickEvent = new EventEmitter<void>();

  ngOnInit() {
    if (this.breadcrumbs) {
      this.breadcrumbs = this.breadcrumbs.map( item => {
        if (typeof item === 'string') {
          return { 'titulo': item };
        } else {
          return item;
        }
      } );
    }
  }

  public onClick() {
    this.clickEvent.emit();
  }

}
