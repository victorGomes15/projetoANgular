import { Component, OnInit } from '@angular/core';
import { smoothlyMenu } from '../../../app.helpers';
import { ConnectContService } from '../../connectcont/connectcont.services';

declare var jQuery:any;

@Component({
  selector: 'topnavbar',
  providers:[
  ],
  templateUrl: 'topnavbar.template.html'
})
export class TopNavbarComponent implements OnInit {

  usuarioLogado = '';

  constructor(
    private connectContService: ConnectContService
  ) {}

  ngOnInit() {
    this.connectContService.getUsuario().subscribe(
      data => {
        this.usuarioLogado = data.nome;
      }
    );
  }

  toggleNavigation(): void {
    jQuery("body").toggleClass("mini-navbar");
    smoothlyMenu();
  }

  logout():void{
  }

}
