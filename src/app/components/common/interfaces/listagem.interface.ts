export interface ListagemPage {
    btnFiltrar(): void;
    btnLimpar(): void;
}

export function instanceOfListagemPage(object: any): boolean {
    if (object) {
        return 'btnFiltrar' in object && 'btnLimpar' in object;
    }
    return false;
}
