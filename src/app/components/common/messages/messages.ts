export class Messages {
    public static readonly MENSAGENS_COM_USUARIO_INATIVO = 'Existem Mensagens Recebidas sem destinatário vinculado,' +
     'verifique através da opção Nenhum do grupo Filtro/Destinatário, sugerimos que um colaborador com acesso total ' +
     'responda as Mensagens Recebidas. Provavelmente o colaborador responsável pela Mensagem Recebida foi inativado no Painel de Controle.';

    public static readonly SACS_COM_USUARIO_INATIVO = 'Existem SACs sem responsável vinculado, verifique através da opção ' +
     'Nenhum do grupo Filtrar/Responsável e transfira o processo para um novo Responsável. Provavelmente o colaborador ' +
     'responsável pelo processo foi inativado no Painel de Controle.';

    public static readonly MENSAGEM_EMPTY_RESULT_USUARIO_ACESSO_CUSTOMIZADO = 'Não há itens a serem listados conforme permissões do' +
    ' colaborador logado.';

    public static readonly SAC_JA_FOI_RESPONDIDO = 'O sac já foi respondido.';
    public static readonly SAC_JA_FOI_CONCLUIDO = 'O sac já foi concluído.';
    public static readonly SAC_SITUACAO_RESPONDIDO = 'Respondido';
    public static readonly SAC_SITUACAO_CONCLUIDO = 'Concluído';
    public static readonly DEPARTAMENTO_NAO_INFORMADO = 'O departamento não foi informado.';
    public static readonly RESPONSAVEL_NAO_INFORMADO = 'O responsável não foi informado.';
    public static readonly SAC_NAO_REDIRECIONADO = 'O sac não foi redirecionado.';

    public static readonly REGISTROS_ENVIADOS_PARA_LIXEIRA_SUCESSO = 'Registros enviados para lixeira, para excluí-los ' +
    'definitivamente acesse o menu Lixeira/Itens Excluídos.';

}
