import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { SessionService } from '../_service/session.service';

declare var jQuery: any;

@Component({
  selector: 'icons-slider',
  templateUrl: 'slide.template.html',
  styleUrls: ['./slide.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SlideComponent implements OnInit, AfterViewInit {

  @Input() id: string = UUID.UUID();

  top = 75;
  itens = [];
  listaDicas = [];

  constructor(
    public sessionService
      : SessionService,
    private ref
      : ChangeDetectorRef,
  ) { }

  @ViewChild('dica') dica;
  @ViewChild('dicas') dicasTemplate;

  ngOnInit() {
    this.sessionService.register(this.id, this);
    this.reload();
  }

  reload() {
    this.resetTabs();
    this.addTab({
      tag: 'dica',
      iconClasses: [ 'fa-lightbulb-o' ],
      conteudoTemplate: this.dica,
    });
    this.addTab({
      tag: 'dica2',
      iconClasses: [ 'fa-lightbulb-o' ],
      conteudoTemplate: this.dica,
    });
    this.addTab({
      tag: 'dica3',
      iconClasses: [ 'fa-lightbulb-o' ],
      conteudoTemplate: this.dica,
    });
    this.setDicasTemplate(this.dicasTemplate, false);
  }

  refreshTop(top) {
    this.top = top;
    this.refresh();
  }

  refresh() {
    jQuery(`#${this.id} .icons-slider`).css('top', this.top + 'px');
  }

  ngAfterViewInit() {
    this.refresh();
    let maxSize = 0;
    jQuery(`#${this.id} .slider-item`).each((idx, element) => {
      const height = Math.min(jQuery('.spin-icon', element).outerHeight(), jQuery('.slider-item-body', element).outerHeight());
      jQuery('.slider-item-show', element).css('min-height', `${height}px`);
      jQuery('.slider-item-header', element).css('min-height', `${height}px`);
      jQuery(element).css('min-height', `${height}px`);
      jQuery('.slider-item-body > .alert', element).css('min-height', `${height}px`);
      const offset = jQuery('.slider-item-body', element).offset();
      if (offset) {
        maxSize = Math.max(maxSize, offset.top + jQuery('.slider-item-body', element).outerHeight());
      }
    });
    const off = jQuery(`#${this.id} .slider-itens`).offset();
    if (off && (maxSize > 0)) {
      if (jQuery('.internal-wrapper').height() < maxSize) {
        jQuery('.internal-wrapper').css('min-height', `${maxSize}px`);
      // } else {
      //   jQuery('.internal-wrapper').css('min-height', jQuery('.internal-wrapper').height() + 'px');
      }
      maxSize = maxSize - off.top;
      jQuery(`#${this.id} .slider-itens`).css('min-height', `${maxSize}px`);
    }
  }

  toggle(itemId) {
    const slider = jQuery(`#${this.id}`);
    const active = !jQuery(`.slider-item.show:has(.${itemId})`, slider).length;
    jQuery(`.slider-item:has(.${itemId})`, slider).toggleClass('show');
    const $itemAtivo = jQuery(`.slider-item:has(.${itemId})`, slider);
    if (active) {
      let z = 2000;
      $itemAtivo.css('z-index', z);
      jQuery(`.slider-item`, slider).toggleClass('active', false);
      jQuery(`.slider-item:has(.${itemId}) ~ .slider-item`, slider).each((idx, item) => {
        const $item = jQuery(item);
        $item.toggleClass('show', true);
        z--;
        $item.css('z-index', z);
      });
    } else {
      const noActive = !jQuery(`.slider-item.active:not(:has(.${itemId}))`, slider).length;
      jQuery(`.slider-item:has(.untogglable):not(.show ~ *)`, slider).toggleClass('show', false);
      if (noActive) {
        jQuery(`.slider-item.show:not(:has(.${itemId})):not(.untogglable):last`, slider).toggleClass('active', true);
      }
    }
    $itemAtivo.toggleClass('active', active);
  }

  hide(tag) {
    this.itens.filter( item => item.tag === tag ).forEach( item => item.show = false );
    this.ref.detectChanges();
  }

  hideAll() {
    this.itens.forEach( item => item.show = false );
    this.ref.detectChanges();
  }

  setListaDicas(dicas, show = true) {
    this.listaDicas = dicas;
    if (show) {
      this.show('dica');
    }
    this.ref.detectChanges();
  }

  setDicasTemplate(dicas, show = true, tag = 'dica') {
    this.itens.filter( item => item.tag === tag )
      .forEach( item => {
        item.context.customBody.context.conteudo.context['dicas'] = { context: {} };
        item.context.customBody.context.conteudo.context['dicas']['template'] = dicas;
        item.show = show;
      } );
    this.ref.detectChanges();
  }

  show(tag) {
    this.itens.filter( item => item.tag === tag ).forEach( item => item.show = true );
    this.ref.detectChanges();
  }

  getItens() {
    return this.itens;
  }

  public resetTabs() {
    this.itens = [];
    this.ref.detectChanges();
  }

  public addTab(custom) {
    const localItens = this.getItens();
    const item = { context: {
      customHeader: { context: {
        iconClasses: {},
        iconClassesTag: {}
      } },
      customBody: { context: { conteudo: { context: { } } } }
    } };
    item['id'] = UUID.UUID();
    item['tag'] = custom.tag;
    if (this.itens.map(itemAtual => itemAtual.tag).includes(item['tag'])) {
      return false;
    }
    if (custom.template) {
      item['template'] = custom.template;
    }
    if (custom.show === undefined) {
      item['show'] = false;
    } else {
      item['show'] = custom.show;
    }
    item.context.customHeader.context['itemTag'] = item['tag'];
    if (custom.headerTemplate) {
      item.context.customHeader['template'] = custom.headerTemplate;
    } else {
      item.context.customHeader.context.iconClasses['fa'] = true;
      if (custom.iconClasses) {
        custom.iconClasses.forEach(element => {
          item.context.customHeader.context.iconClasses[element] = true;
        });
      }
      item.context.customHeader.context.iconClassesTag['spin-icon'] = true;
      item.context.customHeader.context.iconClassesTag[item['tag']] = true;
      if (custom.iconClassesTag) {
        custom.iconClassesTag.forEach(element => {
          item.context.customHeader.context.iconClassesTag[element] = true;
        });
      }
    }
    if (custom.conteudoTemplate) {
      item.context.customBody.context.conteudo['template'] = custom.conteudoTemplate;
    }
    this.itens.push(item);
    return item['id'];
  }
}
