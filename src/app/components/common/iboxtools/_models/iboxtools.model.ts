export interface IIboxToolsConfig {
    dropdownMenu: IDropdownMenuItem[];

    hasCollapse: () => boolean;
    hasDropdown: () => boolean;
    isClosable: () => boolean;
}

export interface IIboxToolsConfigParameter {
    dropdownMenu?: IDropdownMenuItem[];

    hasCollapse?: () => boolean;
    hasDropdown?: () => boolean;
    isClosable?: () => boolean;
    hasCollapseAll?: () => boolean;
}

export class IDropdownMenuItem {
    titulo = '';
    extraClasses: string[] = [];

    acao: () => void = () => {};
}

/** configuração padrão para componente IboxTools */
export class IboxToolsConfig implements IIboxToolsConfig {
    public dropdownMenu = [];

    public hasCollapse = () => true;
    public hasDropdown = () => (this.dropdownMenu || []).length > 0;
    public isClosable = () => false;
}
