import { AfterViewChecked, AfterViewInit, Component, Input, SimpleChanges } from '@angular/core';
import { IIboxToolsConfig, IIboxToolsConfigParameter, IboxToolsConfig } from './_models/iboxtools.model';
import { UUID } from 'angular2-uuid';

declare var jQuery: any;

/** botões do canto superior direito da caixa ibox (ao lado do titulo) */
@Component({
  selector: 'app-iboxtools',
  templateUrl: 'iboxtools.template.html',
  styleUrls: ['./iboxtools.component.scss']
})
export class IboxtoolsComponent implements AfterViewInit {

  _config: IIboxToolsConfigParameter = new IboxToolsConfig();

  $element;

  @Input()
  public set collapseAll(value) {
    this._config.hasCollapseAll = () => value;
  }

  public get collapseAll() {
    return this._config.hasCollapse();
  }

  @Input()
  id = UUID.UUID();

  @Input()
  public get config(): IIboxToolsConfigParameter {
    return this._config;
  }

  ngAfterViewInit(): void {
    this.$element = jQuery('.' + this.id);
    let $src;
    if (this.$element.parents('div.ibox-title').length !== 0) {
      $src = this.$element.parents('div.ibox-title');
    } else {
      $src = this.$element;
    }
    $src.click((event) => {
      if (this.collapseAll) {
        this.collapse(event);
      }
    });
  }

  public set config(value) {
    this._config = value;
    const allConfig: IIboxToolsConfig = Object.assign(new IboxToolsConfig(), this._config);
    Object.assign(this._config, allConfig);
  }

  public collapse(event: Event): void {
    event.preventDefault();
    const ibox = this.collapseAll ? jQuery(this.$element).parents('div.ibox') : jQuery(event.srcElement).parents('div.ibox');
    const content = jQuery('.ibox-content', ibox);
    content.slideToggle(200);
    ibox.toggleClass('border-bottom');
    setTimeout(function () {
      ibox.resize();
      ibox.find('[id^=map-]').resize();
    }, 50);
  }

  public close(e): void {
    e.preventDefault();
    const ibox = jQuery(event.srcElement).parents('div.ibox');
    ibox.remove();
  }


}
