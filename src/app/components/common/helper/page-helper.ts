import { SessionService } from '../_service/session.service';
import { SlideComponent } from '../slide/slide.component';

declare var jQuery: any;

export class PageHelper {
    public static scrollTop(sessionService: SessionService) {
        jQuery('html,body').animate({ scrollTop: 0 }, 'slow');
        sessionService.waitOne('layout').subscribe(layout => layout.sendEvent('scrollToTop'));
    }

    public static dicas(sessionService: SessionService, dicas) {
        sessionService.waitOne('slider1').subscribe((slider: SlideComponent) => {
            slider.setDicasTemplate(dicas);
            // slider.setDicasTemplate(dicas, undefined, 'dica2');
            // slider.setDicasTemplate(dicas, undefined, 'dica3');
        });
    }

    public static hiddenScroll(sessionService: SessionService, hide: boolean) {
        sessionService.waitOne('layout').subscribe(layout => layout.sendEvent('hiddenScroll', { hide: hide }));
    }
}
