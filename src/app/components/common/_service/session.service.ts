import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs/';

declare var jQuery: any;

@Injectable()
export class SessionService {

  public components: Map<string, any> = new Map();
  public subs: Map<string, Subject<any>> = new Map();

  reload() {
    this.components.forEach( component => this.reloadComponent(component) );
  }

  reloadComponent(component) {
    // component.listenerSubject = undefined;
    // component.listenerObservable = undefined;
    if (component.reload) {
      component.reload();
    }
    // component.reloadSubject = new Subject();
    // component.reloadSubject.asObservable().subscribe( listener => listener.next(component) );
}

  waitOne(key) {
    if (this.components.has(key)) {
      const value = this.components.get(key);
      if (this.subs.has(key)) {
        const sub: Subject<any> = this.subs.get(key);
        sub.next(value);
        sub.complete();
        this.subs.delete(key);
      }
      return Observable.of(value);
    } else {
      if (!this.subs.has(key)) {
        this.subs.set(key, new Subject());
      }
      const sub: Subject<any> = this.subs.get(key);
      return sub.asObservable();
    }
  }

  unregister(key) {
    const value = this.components.get(key);
    if (value['referencias']) {
      const referencias = value['referencias'];
      referencias.delete({ referenciador: this.components, referencia: key });
    }
    this.components.delete(key);
    if (this.subs.has(key)) {
      const sub: Subject<any> = this.subs.get(key);
      this.subs.delete(key);
      sub.error('unregister');
    }
  }

  register(key, value) {
    if (this.components.has(key)) {
      this.unregister(key);
    }
    this.components.set(key, value);
    if (!value['referencias']) {
      value['referencias'] = new Set();
    }
    const referencias = value['referencias'];
    referencias.add({ referenciador: this.components, referencia: key });
    if (this.subs.has(key)) {
      const sub: Subject<any> = this.subs.get(key);
      sub.next(value);
      sub.complete();
      this.subs.delete(key);
    }
  }
}
