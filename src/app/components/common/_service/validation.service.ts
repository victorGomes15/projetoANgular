import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertService } from '../../alert/_services';
import { ValidationSelector } from '../../../login/_model/validation.model';

@Injectable()
export class ValidationService {

  constructor(
    private _alertService: AlertService,
  ) { }

  private normalizeParameters(parameters?: {events?: any, components?: any, condicoes?: any, create?: any}) {
    return {
        events      : (parameters || {}).events,
        components  : (parameters || {}).components,
        condicoes   : (parameters || {}).condicoes,
        create      : (parameters || {}).create || false
    };
  }

  addValidCallbackByParameters(
      origin: ValidationSelector, formulario: NgForm,
      parameters?: {events?: any, components: any, condicoes: any}
    ) {
        parameters = this.normalizeParameters(parameters);
        parameters['create'] = undefined;
        const selecionadas = this.validationFor(origin, parameters);
        this.addValidCallback(
            selecionadas,
            () => {
                return !(
                    formulario.controls[parameters.components] &&
                    formulario.controls[parameters.components].errors &&
                    formulario.controls[parameters.components].errors[parameters.condicoes]
                ) ;
            }
        );
    }

  addValidCallback(origin: ValidationSelector, callback: () => boolean) {
    const oldCallback = origin.validCallback;
    origin.validCallback    = () => {
        return oldCallback() && callback();
    };
  }

  validate(origin: ValidationSelector) {
    origin.valid = origin.validCallback();
    return origin.valid;
  }

  showMessages(origin: ValidationSelector) {
    this._alertService.clear();
    origin.validations
        .filter(  validation  => !validation.valid )
        .forEach( validation  => this._alertService.addError(validation.mensagem) );
  }

  isValid(origin: ValidationSelector, parameters?: {events?: any, components?: any, condicoes?: any}) {
    const validation = this.validationFor(origin, this.normalizeParameters(parameters));
    if (validation.validations.length === 0) {
        return this.validate(validation);
    } else {
        return !validation.validations
                .map(     (validationItem: ValidationSelector) => this.validate(validationItem) )
                .includes(false);
    }
  }

  hasError(origin: ValidationSelector, parameters?: {events?: any, components?: any, condicoes?: any}) {
        const validation = this.validationFor(origin, this.normalizeParameters(parameters));
        if (validation.validations.length === 0) {
            return !this.validate(validation);
        } else {
        const localValidations = validation.validations
                .map(     (validationItem: ValidationSelector) => this.validate(validationItem) );
        return localValidations.includes(false);
        }
  }

  isAllError(origin: ValidationSelector, parameters?: {events?: any, components?: any, condicoes?: any}) {
    const validation = this.validationFor(origin, this.normalizeParameters(parameters));
      if (validation.validations.length === 0) {
          return !this.validate(validation);
      } else {
        const localValidations = validation.validations
                  .map(     (validationItem: ValidationSelector) => this.validate(validationItem) );
           return !localValidations.includes(true);
      }
  }

  validationFor(
      origin: ValidationSelector,
      parameters?: {events?: any, components?: any, condicoes?: any, create?: boolean}
    ): ValidationSelector {
      let validation  = new ValidationSelector();
      const all = origin.validations.map(x => Object.assign({}, x));
      all.push(Object.assign({}, origin));
      let internalComponents;
      let internalEvents;
      let internalCondicoes;
      const internalCreate = (typeof parameters.create === 'undefined') ? true : parameters.create;
      if (parameters && parameters.components) {
          if (parameters.components instanceof Array) {
              internalComponents = parameters.components;
          } else {
              internalComponents = [ parameters.components ];
          }
      } else {
          internalComponents = new Set(all.map( (validationItem: ValidationSelector) => validationItem.componentForValidation ));
      }
      if (parameters && parameters.events) {
          if (parameters.events instanceof Array) {
              internalEvents = parameters.events;
          } else {
              internalEvents = [ parameters.events ];
          }
      } else {
          internalEvents = new Set(all.map( (validationItem: ValidationSelector) => validationItem.eventForValidation ));
      }
      if (parameters && parameters.condicoes) {
          if (parameters.condicoes instanceof Array) {
              internalCondicoes = parameters.condicoes;
          } else {
              internalCondicoes = [ parameters.condicoes ];
          }
      } else {
          internalCondicoes = new Set(all.map( (validationItem: ValidationSelector) => validationItem.conditionForValidation ));
      }
      internalComponents.forEach(
          component => {
              internalEvents.forEach(
                  event => {
                      internalCondicoes.forEach(
                          condicao => {
                              validation.validations = validation.validations.concat(origin.validations
                                      .filter(
                                          (innerValidation: ValidationSelector) =>
                                                  innerValidation.componentForValidation  === component
                                              &&  innerValidation.eventForValidation      === event
                                              &&  innerValidation.conditionForValidation  === condicao
                                      )
                              );
                          }
                      );
                  }
              );
          }
      );
      if (internalCreate && validation.validations.length === 0) {
          internalComponents.forEach(
              component => {
                  internalEvents.forEach(
                      event => {
                          internalCondicoes.forEach(
                              condicao => {
                                const newValidation = new ValidationSelector();
                                  newValidation   .componentForValidation = component;
                                  newValidation   .eventForValidation     = event;
                                  newValidation   .conditionForValidation = condicao;
                                  validation      .validations.push(newValidation);
                                  origin          .validations.push(newValidation);
                              }
                          );
                      }
                  );
              }
          );
      }
      if (validation.validations.length === 1) {
          validation = validation.validations[0];
      }
      return validation;
  }

  addFrom(origin: ValidationSelector, tela) {
      tela.forEach( (element: {events?, components?, condicoes?, mensagem}) => {
        const selecionados = this.validationFor(origin, {
              events      : element.events,
              components  : element.components,
              condicoes   : element.condicoes,
          });
          if (selecionados.validations.length === 0) {
            selecionados.mensagem = element.mensagem;
          } else {
            selecionados.validations.forEach( selecionado => selecionado.mensagem = element.mensagem );
          }
      });
  }

    mask() {
    }
}
