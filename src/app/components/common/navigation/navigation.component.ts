import { Component, AfterViewInit, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import 'jquery-slimscroll';
import { ConnectContService } from '../../connectcont/connectcont.services';

declare var jQuery:any;

@Component({
  selector: 'navigation',
  templateUrl: 'navigation.template.html'
})

export class NavigationComponent implements OnInit, AfterViewInit{

  usuarioLogado = '';

  constructor(private router: Router, private connectContService: ConnectContService) {}

  ngOnInit() {
    // this.connectContService.getUsuario().subscribe(
    //   data => {
    //     this.usuarioLogado = data.pessoaFisica.nome;
    //   }
    // );
  }

  ngAfterViewInit() {
    jQuery('#side-menu').metisMenu();

    if (jQuery('body').hasClass('fixed-sidebar')) {
      jQuery('.sidebar-collapse').slimscroll({
        height: '100%'
      })
    }
  }

  activeRoute(routename: string): boolean{
    return this.router.url.indexOf(routename) > -1;
  }

  


}
