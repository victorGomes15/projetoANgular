export class Alert implements IMensagemParameter {
    static readonly TIME_DEFAULT = 4000;
    static readonly TIME_FOREVER = -1;

    type: AlertType;
    message: string;
    timeout: any;
    time: any = Alert.TIME_DEFAULT;
    template: any;
}

export enum AlertType {
    Success,
    Error,
    Info,
    Warning
}

/**
 * configuracoes para customizar as acoes do servico de alertas
 *  */
export interface IMensagemParameter {
    type?: AlertType;
    message?: string;
    timeout?: any;
    /** keepAfterRouteChange estipula que a mensagem permanecera apos a mudanca de tela */
    keepAfterRouteChange?: boolean;
    /** time estipula o tempo que a mensagem permanecera na tela */
    time?: number;
    /** template e templateName estipulam como sera o alerta */
    templateName?: string;
    template?;
}

export interface IFonteMensagemParameter extends IMensagemParameter {
    defaults?: IMensagemParameter;
    mensagens?: IMensagemParameter[];
}

/**
 * configuracoes para os componente de alertas
 *  */
export interface IAlertConfigParameter extends IFonteMensagemParameter {
    /**
     * alertClass estipula um css customizado para utilizacao no componente de alertas
     * permitindo identificar o componente individualmente por exemplo para atuar nele via jquery
     *  */
    alertClass?: string;
    /** configs contem as fontes de mensagens utilizadas */
    represa?;
    estaRepresandoAlertas?: boolean;
    temAlertasRepresados?: boolean;
    possuiAlertas?: boolean;

    mostraAlertasRepresados?: () => any;
    represaAlertas?: () => any;
}

/** configs contem as fontes de mensagens utilizadas */
export class RepresaMensagem implements IAlertConfigParameter {
    public static MENSAGEM_DEFAULT = new RepresaMensagem(<IAlertConfigParameter>{
        keepAfterRouteChange: false,
        time: Alert.TIME_DEFAULT,
    })._clear();

    public defaults: IMensagemParameter = {};

    constructor(... other: IAlertConfigParameter[]) {
        Object.assign(this, ... other);
        this.defaults = Object.assign({}, this.defaults);
    }

    /** fonte com represamento de mensagens */
    public represa: IFonteMensagemParameter[] = [ { mensagens: [] } ];

    private _clear(): RepresaMensagem {
        this.represa = undefined;
        delete this.represa;
        return this;
    }

    public get type(): AlertType {
        return this.defaults.type;
    }
    public set type(value: AlertType) {
        this.defaults.type = value;
    }
    public get message(): string {
        return this.defaults.message;
    }
    public set message(value: string) {
        this.defaults.message = value;
    }
    public get timeout() {
        return this.defaults.timeout;
    }
    public set timeout(value) {
        this.defaults.timeout = value;
    }
    public get keepAfterRouteChange(): boolean {
        return this.defaults.keepAfterRouteChange;
    }
    public set keepAfterRouteChange(value: boolean) {
        this.defaults.keepAfterRouteChange = value;
    }
    public get time(): number {
        return this.defaults.time;
    }
    public set time(value: number) {
        this.defaults.time = value;
    }
    public get templateName(): string {
        return this.defaults.templateName;
    }
    public set templateName(value: string) {
        this.defaults.templateName = value;
    }
    public get template() {
        return this.defaults.template;
    }
    public set template(value) {
        this.defaults.template = value;
    }

    public get estaRepresandoAlertas() {
        return this.represa.length > 1;
    }
    public get temAlertasRepresados() {
        const alertasRepresados = this.represa.filter( (value, index) => index !== 0 );
        return this.estaRepresandoAlertas && Object.assign({}, ... alertasRepresados ).mensagens.length;
    }
    public get possuiAlertas() {
        return !!this.mensagens.length;
    }
    public get mensagensExibidas() {
        const primeiro = new RepresaMensagem(this);
        if (this.represa && this.represa[0]) {
            primeiro.represa = [ this.represa[0] ];
        }
        return primeiro;
    }
    public get mensagensRepresadas() {
        const ultimo = new RepresaMensagem(this);
        ultimo.represa = ultimo.represa.filter( fonte => fonte.mensagens.length > 0 ).reverse();
        if (!ultimo.represa[0]) {
            ultimo.represa = [ this.represa[0] ];
        }
        return ultimo;
    }

    public mostraAlertasRepresados() {
        this.represa = [ Object.assign({}, ... this.represa) ];
        return this.represa;
    }
    public represaAlertas() {
        if (!this.estaRepresandoAlertas) {
            this.represa.push( { mensagens: [] } );
        }
    }

    public get mensagens(): IMensagemParameter[] {
        if (this.represa && this.represa[0]) {
            return this.represa[0].mensagens;
        }
        return [];
    }

    public set mensagens(msgs: IMensagemParameter[]) {
        if (this.represa && this.represa[0]) {
            this.represa[0].mensagens = msgs;
        }
    }
}
