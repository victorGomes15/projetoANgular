import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { AlertService } from '../_services';
import { IAlertConfigParameter, RepresaMensagem, Alert, AlertType } from '../_models';


@Component({
    moduleId: module.id,
    selector: 'app-alert',
    templateUrl: 'alert.component.html'
})

export class AlertComponent implements AfterViewInit {

    public configValue: IAlertConfigParameter = new RepresaMensagem();

    @Input('configs')
    public set configs(value: IAlertConfigParameter) {
        const valor = Object.assign( new RepresaMensagem(), { alertClass: 'alertas' }, value );
        this.configValue = Object.assign( (value || new RepresaMensagem()), valor );
    }

    public get template() {
        return this.configValue.template
                || this.templates.get(this.configValue.templateName || 'individual');
    }

    templates: Map<string, any> = new Map();

    constructor( public alertService: AlertService ) {
        this.configs = alertService;
    }

    @ViewChild('individualAlertPopTemplate')
    individualAlertPopTemplate;
    @ViewChild('listAlertPopTemplate')
    listAlertPopTemplate;

    ngAfterViewInit() {
        this.templates.set('individual', this.individualAlertPopTemplate);
        this.templates.set('list', this.listAlertPopTemplate);
    }

    removeAlert(alert: Alert) {
        this.configValue.mensagens = this.configValue.mensagens.filter(x => x !== alert);
    }

    filterType(mensagens, type) {
        return mensagens.filter( mensagem => mensagem.type === type );
    }

    types() {
        return [ AlertType.Success, AlertType.Error, AlertType.Info, AlertType.Warning ];
    }

    isSuccess(type) {
        return type === AlertType.Success;
    }
    isError(type) {
        return type === AlertType.Error;
    }
    isInfo(type) {
        return type === AlertType.Info;
    }
    isWarning(type) {
        return type === AlertType.Warning;
    }
}
