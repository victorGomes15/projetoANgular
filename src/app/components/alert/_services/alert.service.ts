import { Injectable } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs/';
import { SessionService } from '../../common/_service/session.service';
import { RepresaMensagem, IAlertConfigParameter, Alert, AlertType, IMensagemParameter, IFonteMensagemParameter } from '../_models';

@Injectable()
export class AlertService extends RepresaMensagem {

    private routeChange: Observable<any>;

    constructor(private router: Router, public sessionService: SessionService) {
        super();
        // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
        const sub = new Subject();
        this.routeChange = sub.asObservable();
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                // only keep for a single route change
                sub.next(event);
            }
        });
    }

    /**
     * cria nova configuracao que simula comportamentos do contadoronline
     * sem "represamento" de mensagens (ver metodo configContadorOnlineStyle)
     * - exibe em forma de lista
     * - alertas fixos e nao somem
     *  */
    public configListStyle(... configs: IAlertConfigParameter[]): RepresaMensagem {
        return new RepresaMensagem( this,
            <IAlertConfigParameter>{ templateName: 'list', time: Alert.TIME_FOREVER, keepAfterRouteChange: false }, ... configs );
    }

    /**
     * cria nova configuracao que simula comportamentos do contadoronline
     * onde tudo acontece de uma vez ao acionar um botao
     * para isso acumulamos (represamos) as mensagens
     * em uma fonte alternativa de mensagens ( configs )
     * - represamento de mensagens
     * - exibe em forma de lista
     * - alertas fixos e nao somem
     *  */
    public configContadorOnlineStyle(... configs: IAlertConfigParameter[]): RepresaMensagem {
        const represa = this.configListStyle(... configs);
        represa.represaAlertas();
        return represa;
    }

    /**
     * limpa mensagens antigas e mostra mensagem do tipo AlertType.Success, recebe:
     * message o texto conteudo da mensagem
     * configs, opcional, com configuracoes do componente de exibicao dos alertas (ver IAlertConfigs)
     * moreConfigs, opcional, com configuracoes para a execucao desse metodo (ver IAlertConfig)
     */
    success(message: string, ... configs: IAlertConfigParameter[]) {
        this.alert(AlertType.Success, message, ... configs);
    }

    /**
     * limpa mensagens antigas e mostra mensagem do tipo AlertType.Error, recebe:
     * message o texto conteudo da mensagem
     * configs, opcional, com configuracoes do componente de exibicao dos alertas (ver IAlertConfigs)
     * moreConfigs, opcional, com configuracoes para a execucao desse metodo (ver IAlertConfig)
     */
    error(message: string, ... configs: IAlertConfigParameter[]) {
        this.alert(AlertType.Error, message, ... configs);
    }

    /**
     * limpa mensagens antigas e mostra mensagem do tipo AlertType.Info, recebe:
     * message o texto conteudo da mensagem
     * configs, opcional, com configuracoes do componente de exibicao dos alertas (ver IAlertConfigs)
     * moreConfigs, opcional, com configuracoes para a execucao desse metodo (ver IAlertConfig)
     */
    info(message: string, ... configs: IAlertConfigParameter[]) {
        this.alert(AlertType.Info, message, ... configs);
    }

    /**
     * limpa mensagens antigas e mostra mensagem do tipo AlertType.Warning, recebe:
     * message o texto conteudo da mensagem
     * configs, opcional, com configuracoes do componente de exibicao dos alertas (ver IAlertConfigs)
     * moreConfigs, opcional, com configuracoes para a execucao desse metodo (ver IAlertConfig)
     */
    warn(message: string, ... configs: IAlertConfigParameter[]) {
        this.alert(AlertType.Warning, message, ... configs);
    }

    private toAlert(type: AlertType, message: string, config: IAlertConfigParameter): IMensagemParameter {
        const alert: IMensagemParameter = { type: type, message: message };
        alert.time = config.time;
        return alert;
    }

    /**
     * mostra mensagem do tipo AlertType.Success, recebe:
     * message o texto conteudo da mensagem
     * configs, opcional, com configuracoes do componente de exibicao dos alertas (ver IAlertConfigs)
     * moreConfigs, opcional, com configuracoes para a execucao desse metodo (ver IAlertConfig)
     */
    addSuccess(message: string, ... configs: IAlertConfigParameter[]) {
        this.addAlert(AlertType.Success, message, ... configs);
    }

    /**
     * mostra mensagem do tipo AlertType.Error, recebe:
     * message o texto conteudo da mensagem
     * configs, opcional, com configuracoes do componente de exibicao dos alertas (ver IAlertConfigs)
     * moreConfigs, opcional, com configuracoes para a execucao desse metodo (ver IAlertConfig)
     */
    addError(message: string, ... configs: IAlertConfigParameter[]) {
        this.addAlert(AlertType.Error, message, ... configs);
    }

    /**
     * mostra mensagem do tipo AlertType.Info, recebe:
     * message o texto conteudo da mensagem
     * configs, opcional, com configuracoes do componente de exibicao dos alertas (ver IAlertConfigs)
     * moreConfigs, opcional, com configuracoes para a execucao desse metodo (ver IAlertConfig)
     */
    addInfo(message: string, ... configs: IAlertConfigParameter[]) {
        this.addAlert(AlertType.Info, message, ... configs);
    }

    /**
     * mostra mensagem do tipo AlertType.Warning, recebe:
     * message o texto conteudo da mensagem
     * configs, opcional, com configuracoes do componente de exibicao dos alertas (ver IAlertConfigs)
     * moreConfigs, opcional, com configuracoes para a execucao desse metodo (ver IAlertConfig)
     */
    addWarn(message: string, ... configs: IAlertConfigParameter[]) {
        this.addAlert(AlertType.Warning, message, ... configs);
    }

    private addAlert(type: AlertType, message: string, ... configs: IAlertConfigParameter[]) {
        const config = new RepresaMensagem( this, RepresaMensagem.MENSAGEM_DEFAULT, ... configs );
        const alert: IMensagemParameter = this.toAlert(type, message, config);
        this.sessionService.waitOne('layout').subscribe( layout => this.routeChange.take(2).subscribe( next => {
            this.hide(layout, config);
            if (config.keepAfterRouteChange) {
                config.keepAfterRouteChange = false;
            } else {
                this.remove(alert, ... configs);
            }
        } ) );
        if (config.template) {
            alert.template = config.template;
        }
        config.mensagensRepresadas.mensagens.push(alert);
        if (alert.time > 0) {
            this.setTimeOut(alert);
        }
    }

    private alert(type: AlertType, message: string, ... configs: IAlertConfigParameter[]) {
        this.clear(... configs);
        this.addAlert(type, message, ... configs);
    }

    private _clearTimeOut(... configs: IAlertConfigParameter[]) {
        const config = new RepresaMensagem( this, RepresaMensagem.MENSAGEM_DEFAULT, ... configs );
        config.mensagensRepresadas.mensagens.forEach(alert => {
            if (alert.timeout) {
                clearTimeout(alert.timeout);
            }
        });
    }

    public remove(alert: IMensagemParameter, ... configs: IAlertConfigParameter[]) {
        const config = new RepresaMensagem( this, RepresaMensagem.MENSAGEM_DEFAULT, ... configs );
        config.mensagensRepresadas.mensagens = config.mensagensRepresadas.mensagens.filter(obj => obj !== alert);
    }

    private setTimeOut(alert: IMensagemParameter) {
        alert.timeout = setTimeout(() => this.remove(alert), alert.time);
    }

    public clear(... configs: IAlertConfigParameter[]) {
        const config = new RepresaMensagem( this, RepresaMensagem.MENSAGEM_DEFAULT, ... configs );
        // clear alerts
        this._clearTimeOut(... configs);
        config.mensagensRepresadas.mensagens = [];
    }

    public hide(layout, alertConfig) {
        if (layout && layout.hide) {
            layout.hide(alertConfig);
        }
    }
}
