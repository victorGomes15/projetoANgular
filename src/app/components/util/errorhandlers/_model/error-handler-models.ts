import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler } from '../_interfaces/error.interface';
import { IAlertConfigParameter } from '../../../alert/_models';

// 400
export class ResourceBadRequestException extends Error {
    constructor(message: string) {
        super(message);
    }
}

// 401
export class ResourceUnauthorizedException extends Error {
    constructor(message: string) {
        super(message);
    }
}

// 403
export class ResourceAccessDeniedException extends Error {
    constructor(message: string) {
        super(message);
    }
}

// 404
export class ResourceNotFoundException extends Error {
    constructor(message: string) {
        super(message);
    }
}

// 422
export class ResourceBusinessException extends Error {
    constructor(message: string) {
        super(message);
    }
}

// 500
export class ServerException extends Error {
    constructor(message: string) {
        super(message);
    }
}

export interface IErrorHandlerConfig {
    defaultMsg?: string;
    permissionMessages?: ((data) => string)[];
    customHandlers?: Map<number, ErrorHandler>;
    alertConfigs?: IAlertConfigParameter;
    navigation?: string[];
}
