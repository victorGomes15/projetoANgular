import { ErrorHandler } from '../_interfaces/error.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { ResourceBusinessException } from '../_model/error-handler-models';

export class ResourceBusinessExceptionHandler implements ErrorHandler {

    getError(error: HttpErrorResponse): Error {
        return new ResourceBusinessException(error.error.error || error.error.detail || 'Dados inválidos.');
    }

}
