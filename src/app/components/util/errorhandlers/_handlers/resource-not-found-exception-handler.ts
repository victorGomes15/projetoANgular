import { ErrorHandler } from '../_interfaces/error.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { ResourceNotFoundException } from '../_model/error-handler-models';

export class ResourceNotFoundExceptionHandler implements ErrorHandler {

    getError(error: HttpErrorResponse): Error {
        return new ResourceNotFoundException(error.error.error || 'Recurso não encontrado.');
    }
}
