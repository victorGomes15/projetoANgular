import { ErrorHandler } from '../_interfaces/error.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { ResourceUnauthorizedException } from '../_model/error-handler-models';

export class ResourceUnauthorizedExceptionHandler implements ErrorHandler {

    getError(error: HttpErrorResponse): Error {
        return new ResourceUnauthorizedException('Acesso Negado!');
    }
}
