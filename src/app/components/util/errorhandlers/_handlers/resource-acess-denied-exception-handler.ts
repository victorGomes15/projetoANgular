import { ErrorHandler } from '../_interfaces/error.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { ResourceAccessDeniedException } from '../_model/error-handler-models';

export class ResourceAccessDeniedExceptionHandler implements ErrorHandler {

    getError(error: HttpErrorResponse): Error {
        return new ResourceAccessDeniedException('Você não possui permissão para acessar esta rotina.');
    }
}
