import { ErrorHandler } from '../_interfaces/error.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { ResourceBadRequestException } from '../_model/error-handler-models';

export class ResourceBadRequestExceptionHandler implements ErrorHandler {

    public static readonly DEFAULT_MESSAGE = 'Dados inválidos.';

    getError(error: HttpErrorResponse): Error {
        return new ResourceBadRequestException(error.error.error_description || ResourceBadRequestExceptionHandler.DEFAULT_MESSAGE);
    }

}
