import { ErrorHandler } from '../_interfaces/error.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { ServerException } from '../_model/error-handler-models';

export class ResourceServerExceptionHandler implements ErrorHandler {

    getError(error: HttpErrorResponse): Error {
        return new ServerException('Servidor Indisponível.');
    }
}
