import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/';
import { AcessoNegadoService } from '../../../security/acessoNegado/acessoNegado.service';
import { AlertService } from '../../alert/_services';
import { ResourceAccessDeniedExceptionHandler } from './_handlers/resource-acess-denied-exception-handler';
import { ResourceBadRequestExceptionHandler } from './_handlers/resource-bad-request-exception-handler';
import { ResourceBusinessExceptionHandler } from './_handlers/resource-business-exception-handler';
import { ResourceNotFoundExceptionHandler } from './_handlers/resource-not-found-exception-handler';
import { ResourceUnauthorizedExceptionHandler } from './_handlers/resource-unauthorized-exception-handler';
import { ResourceServerExceptionHandler } from './_handlers/server-exception-handler';
import { ErrorHandler } from './_interfaces/error.interface';
import { IErrorHandlerConfig } from './_model/error-handler-models';

@Injectable()
export class ErrorHandlerService {

    handlers: Map<number, ErrorHandler> = new Map<number, ErrorHandler>();

    constructor(
        private alertService: AlertService,
        private acessoNegadoService: AcessoNegadoService,
        private router: Router,
    ) {
        this.handlers.set(0, new ResourceServerExceptionHandler());
        this.handlers.set(400, new ResourceBadRequestExceptionHandler());
        this.handlers.set(401, new ResourceUnauthorizedExceptionHandler());
        this.handlers.set(403, new ResourceAccessDeniedExceptionHandler());
        this.handlers.set(404, new ResourceNotFoundExceptionHandler());
        this.handlers.set(422, new ResourceBusinessExceptionHandler());
        this.handlers.set(500, new ResourceServerExceptionHandler());
    }

    getPermissionErrorHandler(config?: IErrorHandlerConfig) {
        return (error) => this.handlePermissionError(error, config);
    }

    handlePermissionError(error, config?: IErrorHandlerConfig) {
        if (error instanceof HttpErrorResponse && error.status === 403) {
            const messages = config.permissionMessages.map( func => ({ resolve: func }) );
            this.acessoNegadoService.acessoNegado( ... messages );
        } else {
            this.handleError(error, config);
        }
    }

    getErrorHandler(config?: IErrorHandlerConfig) {
        return (error) => {
            this.handleError(error, config);
            return Observable.throw(error);
        };
    }

    handleError(error, config?: IErrorHandlerConfig) {
        const messages = this.getErrorMsgs(error, config);
        if (!config.navigation || !messages.find( msg => msg.rejected )) {
            messages.filter( msg => !msg.rejected )
                .forEach( msg => this.alertService.addError(msg.message, config.alertConfigs) );
        } else {
            messages.filter( msg => msg.rejected )
                .forEach( msg => this.alertService.addError(msg.message, config.alertConfigs, { keepAfterRouteChange: true }) );
            this.router.navigate(config.navigation, { queryParamsHandling: 'merge' });
        }
    }

    getErrorMsgs(error, config?: IErrorHandlerConfig) {
        if (error instanceof HttpErrorResponse && error.status === 422 && error.error.errors) {
            return error.error.errors;
        } else {
            return [ { message: this.getErrorMsg(error, config)} ];
        }
    }

    getErrorMsg(error, config?: IErrorHandlerConfig): string {
        if (error instanceof HttpErrorResponse) {
            if (error.status === 500) {
                return config.defaultMsg;
            }
            if (config.customHandlers) {
                this.handlers.forEach((value, key) => {
                    if (!config.customHandlers.has(key)) {
                        config.customHandlers.set(key, value);
                    }
                });
            } else {
                config.customHandlers = this.handlers;
            }
            const handler = config.customHandlers.get(error.status);
            console.log('Erro EDGE', error);
            return handler ? handler.getError(error).message : config.defaultMsg;
        } else {
            return config.defaultMsg;
        }
    }

    getErrorClass <T extends HttpErrorResponse> (error: T): Error {
        const handler = this.handlers.get(error.status);
        console.log('Erro EDGE', error);
        return handler ? handler.getError(error) : new Error('Erro desconhecido');
    }

}

