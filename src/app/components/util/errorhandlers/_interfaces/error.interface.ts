import { HttpErrorResponse } from '@angular/common/http';

export interface ErrorHandler {
    getError(error: HttpErrorResponse): Error;
}
