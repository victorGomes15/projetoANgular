import { HttpHeaders } from '@angular/common/http';
import { Cookie } from 'ng2-cookies';
import * as oauth2 from '../../../security/oauth2/oauth2.constants';
import * as accepts from '../headers/header-accepts.constants';
import { Pageable } from '../../data-table/_models/paginacao.model';

export class HeaderUtils {

    private static readonly PREFIX = 'resources ';

    public currentPage: number;
    public maxPage: number;
    public total: number;

    private constructor() { }

    public static getContentRange(contentRange: string): HeaderUtils {
        const contentRangeObject = new HeaderUtils();
        if (contentRange && contentRange.startsWith(this.PREFIX)) {
            const indexBarra = contentRange.indexOf('/');
            const paginas = contentRange.substring(this.PREFIX.length, indexBarra);
            const paginasArrays = paginas.split('-');
            contentRangeObject.total = parseInt(contentRange.substring(indexBarra + 1), undefined);
            contentRangeObject.currentPage = parseInt(paginasArrays[0], undefined);
            contentRangeObject.maxPage = parseInt(paginasArrays[1], undefined);
        }
        return contentRangeObject;
    }

    public static getPageable(headers: HttpHeaders): Pageable {
        const contentRangeString: string = headers.get('Content-Range');
        let page: Pageable;
        if (contentRangeString) {
            const contentRangeObject = HeaderUtils.getContentRange(contentRangeString);
            if (contentRangeObject) {
                page = new Pageable();
                page.offset = contentRangeObject.currentPage;
                page.maxOffset = contentRangeObject.maxPage;
                page.total = contentRangeObject.total;
            }
        }
        return page;

    }

    public static getHeaderAuthorization(headers: HttpHeaders): HttpHeaders {
        return headers.append('Authorization', 'Bearer ' + Cookie.get(oauth2.accessToken));
    }

    public static getHeaderWithAuthorization(accept?: string): HttpHeaders {
        let headers = new HttpHeaders();
        if (accept) {
            headers = headers.append('Accept', accept);
        } else {
            headers = headers.append('Accept', accepts.applicationJson);
            headers = headers.append('Content-Type', accepts.applicationJson);
        }
        headers = HeaderUtils.getHeaderAuthorization(headers);
        return headers;
    }

    public static getHeaderJSON(accept?: string): HttpHeaders {
        let headers = new HttpHeaders();
        headers = headers.append('Accept', accepts.applicationJson);
        headers = headers.append('Content-Type', accepts.applicationJson);
        return headers;
    }

    public static getOptionsWithAuthorization(accept?: string) {
        return { headers: HeaderUtils.getHeaderWithAuthorization(accept) };
    }

    public static getHeadersUploadAnexo() {
        let headers =  HeaderUtils.getHeaderWithAuthorization(accepts.applicationJson);
        if (Cookie.get('session_login')) {
            headers = headers.append('X-Usuario-Id', Cookie.get('session_login'));
        } else if (sessionStorage.getItem('session_login')) {
            headers = headers.append('X-Usuario-Id', sessionStorage.getItem('session_login'));
        }
        return { headers: headers };
    }
}
