import { AfterViewInit, Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { SessionService } from '../../common/_service/session.service';
import { DateUtils } from './date-utils';
import { MascaraDirective } from './mascara.directive';

@Directive({
    selector: '[appData]',
    exportAs: 'DataDirective'
})
export class DataDirective extends MascaraDirective implements AfterViewInit {

    public _format = 'date';

    constructor(el: ElementRef, modalService: BsModalService, sessionService: SessionService) {
        super(el, modalService, sessionService);
        this.appMascara = '99/99/9999';
    }

    @Input()
    /** formato do objeto modelo */
    public get format() {
        return this._format;
    }

    public set format(value) {
        this._format = value;
        this.formatChange();
        this.valor = this.transformationOut(this.valor);
    }

    @Input('appData')
    public get appData() {
        return this.valor;
    }

    public set appData(value) {
        this.valor = value;
    }

    @Output()
    private appDataChange = new EventEmitter<string>();

    ngAfterViewInit() {
        super.ngAfterViewInit();
        this.formatChange();
    }

    private formatChange() {
        if (this._format === 'date') {
            this.transformationIn = DateUtils.dateTransformationIn;
            this.transformationOut = DateUtils.dateTransformationOut;
        } else if (this._format === 'datetimeInicio') {
            this.transformationIn = DateUtils.dateTransformationIn;
            this.transformationOut = (value) => DateUtils.dateTimeTransformationOut(value, undefined, DateUtils.FIRST_INSTANT);
        } else if (this._format === 'datetimeFim') {
            this.transformationIn = DateUtils.dateTransformationIn;
            this.transformationOut = (value) => DateUtils.dateTimeTransformationOut(value, undefined, DateUtils.LAST_INSTANT);
        }
    }

    @HostListener('focusoutAfterChange', ['$event'])
    focusoutAfterChangeEvent(event) {
        this.appDataChange.emit(event);
        DateUtils.componentDateIsValid(this);
    }
}
