import { ModalfiltrodatainvalidaComponent } from './modals/modalfiltrodatainvalida/modalfiltrodatainvalida.component';
import { DateComponent } from './_models/date.model';

export class DateUtils {
    static readonly months30 = [ 4, 6, 9, 11 ];
    static readonly fevereiro = 2;

    static readonly FIRST_INSTANT = {
        hora: '00',
        minuto: '00',
        segundo: '00',
        milesimo: '000'
    };

    static readonly LAST_INSTANT = {
        hora: '23',
        minuto: '59',
        segundo: '59',
        milesimo: '999'
    };

    public static componentMonthIsValid(component: DateComponent) {
        const val = component.valor;
        if (val) {
            if (val.replace(/[^0-9]/g, '').length === 6) {
                const isValidDate = DateUtils.isValidMonth(val);
                if (!isValidDate) {
                    component.modal(ModalfiltrodatainvalidaComponent);
                }
                return isValidDate;
            }
        }
        return true;
    }

    public static isValidMonth(value) {
        if (value === '') {
            return true;
        }

        let mes;
        let ano;
        const ExpReg = new RegExp('(0[1-9]|1[012])/[123456789][0-9]{3}');
        const ardt = value.split('/');

        /*Convertendo as String para Number*/
        if (ardt.length === 1) {
            mes = Number(ardt[0]);
        } else if (ardt.length === 2) {
            mes = Number(ardt[0]);
            ano = Number(ardt[1]);
        }

        if (value.search(ExpReg) === -1) {
            return false;
        } else if ((ano <= 1799) || (ano > 9999)) {
            return false;
        }
        return true;
    }

    public static componentDateIsValid(component: DateComponent, value?) {
        const val = value || component.valor;
        const isValidDate = DateUtils.isValidDate(val);
        if (!isValidDate) {
            component.modal(ModalfiltrodatainvalidaComponent);
        }
        return isValidDate;
    }

    public static canConvertDate(value) {
        return !value || !Number.isNaN(new Date(value).valueOf());
    }

    public static isValidDate(value) {
        if (value && value.replace(/[^0-9]/g, '').length === 8) {
            if (value === '') {
                return true;
            }

            let dia;
            let mes;
            let ano;
            const ExpReg = new RegExp('(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[123456789][0-9]{3}');
            const ardt = value.split('/');

            /*Convertendo as String para Number*/
            if (ardt.length === 2) {
                dia = Number(ardt[0]);
                mes = Number(ardt[1]);
            } else if (ardt.length === 3) {
                dia = Number(ardt[0]);
                mes = Number(ardt[1]);
                ano = Number(ardt[2]);
            }

            if (value.search(ExpReg) === -1) {
                return false;
            } else if (DateUtils.months30.includes(mes) && (dia > 30) || (ano <= 1799) || (ano > 9999)) {
                return false;
            } else if (mes === DateUtils.fevereiro) {
                if ((dia > 28) && !(ano % 4 === 0 && ano % 100 !== 0 || ano % 400 === 0)) {
                    return false;
                }
                if ((dia > 29) && (ano % 4 === 0 && ano % 100 !== 0 || ano % 400 === 0)) {
                    return false;
                }
            }
        }

        return true;
    }

    public static dateTimeTransformationOut(valor: string, valorHora?: string, defaultInstant?): string {
        let retorno = DateUtils.dateTransformationOut(valor);
        if (retorno && (valorHora || defaultInstant)) {
            const instant = Object.assign({}, DateUtils.FIRST_INSTANT, defaultInstant);
            let horaNumeros = '';
            if (valorHora) {
                horaNumeros = valorHora.replace(/[^0-9]/g, '');
            }
            instant.hora = horaNumeros.substring(0, 2) || instant.hora;
            instant.minuto = horaNumeros.substring(2, 4) || instant.minuto;
            instant.segundo = horaNumeros.substring(4, 6) || instant.segundo;
            instant.milesimo = horaNumeros.substring(6, 9) || instant.milesimo;
            retorno += 'T' + instant.hora + ':' + instant.minuto + ':' + instant.segundo + '.' + instant.milesimo;
            return retorno;
        }
        return undefined;
    }

    public static dateTransformationOut(valor: string): string {
        if (valor) {
            const valorNumeros = valor.replace(/[^0-9]/g, '');
            const dia = valorNumeros.substring(0, 2);
            const mes = valorNumeros.substring(2, 4);
            const ano = valorNumeros.substring(4, 8);
            return valorNumeros ? ano + '-' + mes + '-' + dia : valorNumeros;
        }
        return undefined;
    }

    public static dateTransformationIn(value: string): string {
        if (value) {
            const data = DateUtils.dateDesmembration(value);
            let val = data.dia ? data.dia + '/' : data.dia;
            val += data.mes ? data.mes + '/' : data.mes;
            val += data.ano;
            return val;
        }
        return undefined;
    }

    public static dateDesmembration(value: string) {
        if (value) {
            const val: string = value.replace(/[^0-9]/g, '');
            const retorno = {
                dia: val.substring(6, 8),
                mes: val.substring(4, 6),
                ano: val.substring(0, 4),
                hora: val.substring(8, 10),
                minuto: val.substring(10, 12),
                segundo: val.substring(12, 14),
                milesimo: val.substring(14)
            };
            return retorno;
        }
        return undefined;
    }

    public static isBeforeDate(dataAntes: string, dataDepois: string): boolean {
        return new Date(dataAntes) <= new Date(dataDepois);
      }
}
