import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap';
import { SessionService } from '../../common/_service/session.service';
import { DateUtils } from './date-utils';
import { ModalfiltrodatainvalidaComponent } from './modals/modalfiltrodatainvalida/modalfiltrodatainvalida.component';
import { DatapickerOptions } from './_models/date.model';

declare var jQuery: any;

@Component({
    moduleId: module.id,
    selector: 'app-datapicker',
    styles: [ `
        .form-control[readonly] {
            background-color: unset;
            opacity: unset;
        }
    ` ],
    template: `
        <div #datapicker class="input-append date input-group datapicker">
            <ng-content></ng-content>
            <input #input type="text" class="form-control" [(ngModel)]="model" required
                    readonly />
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        </div>
    `,
})
export class DatapickerComponent implements AfterViewInit {

    private modelValue;
    private $element;
    private optionsValue: DatapickerOptions;
    // atualmente componente esta sendo utilizado somente com mes , futuramente podera ser extendido
    private month = true;

    private readonly optionsDefault: DatapickerOptions = {
        language: 'pt-BR',
        autoclose: true,
        forceParse: false
    };

    private readonly optionsMonth: DatapickerOptions = {
        minViewMode: 1,
        format: 'mm/yyyy',
    };

    constructor(private modalService: BsModalService, public sessionService: SessionService, private datepipe: DatePipe) {
    }

    @Input('options')
    public get options() {
        return this.optionsValue;
    }

    public set options(value: DatapickerOptions) {
        const options = Object.assign({}, this.optionsDefault, this.optionsMonth, value);
        this.optionsValue = Object.assign(value, options);
        this.$element.datepicker(this.optionsValue).on('changeDate', e => this.changeDateEvent(e));
    }

    @Input('model')
    public get model() {
        return this.modelValue;
    }

    public set model(value) {
        this.modelValue = value;
        this.modelChange.emit(value);
    }

    @Output()
    private modelChange = new EventEmitter<string>();

    @Output()
    private changeDate = new EventEmitter<any>();

    @ViewChild('datapicker') private datapicker: ElementRef;
    @ViewChild('input') public input: FormControl;

    ngAfterViewInit() {
        this.$element = jQuery(this.datapicker.nativeElement);
        this.sessionService.waitOne('layout').subscribe( layout => {
            this.modalService.onShow.subscribe( next => layout.sendEvent('startModal') );
            this.modalService.onHide.subscribe( next => layout.sendEvent('endModal') );
        } );
        this.options = {};
    }

    public changeDateEvent(event) {
        this.setModelAsDate(event.date);
        this.changeDate.emit(this.model);
    }

    public setModelAsDate(value: Date) {
        const stringValue = this.datepipe.transform(value, 'MM/yyyy');
        this.$element.datepicker('update', value);
        this.model = stringValue;
    }

    public isValid(value) {
        if (this.month && value) {
            const isValidDate = DateUtils.isValidMonth(value);
            if (!isValidDate) {
                this.modalService.show(ModalfiltrodatainvalidaComponent, { backdrop: 'static' });
            }
            return isValidDate;
        }
        return true;
    }
}
