import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalfiltrodatainvalidaComponent } from './modalfiltrodatainvalida.component';

describe('ModalfiltrodatainvalidaComponent', () => {
  let component: ModalfiltrodatainvalidaComponent;
  let fixture: ComponentFixture<ModalfiltrodatainvalidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalfiltrodatainvalidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalfiltrodatainvalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
