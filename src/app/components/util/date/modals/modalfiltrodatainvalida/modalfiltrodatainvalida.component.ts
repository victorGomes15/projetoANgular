import { Component, OnInit } from '@angular/core';
import { BsModalRef } from '../../../../../../../node_modules/ngx-bootstrap';

@Component({
  selector: 'app-modalfiltrodatainvalida',
  templateUrl: './modalfiltrodatainvalida.component.html',
  styleUrls: ['./modalfiltrodatainvalida.component.scss']
})
export class ModalfiltrodatainvalidaComponent implements OnInit {

  modal: BsModalRef;

  constructor(private modalRef: BsModalRef) {
    this.modal = modalRef;
  }

  ngOnInit() {
  }

  confirmar() {
    this.modal.hide();
  }

}
