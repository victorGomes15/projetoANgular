import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { DatapickerComponent } from './datapicker.component';
import { DataDirective } from './data.directive';
import { MascaraDirective } from './mascara.directive';
import { MesDirective } from './mes.directive';
import { ModalfiltrodatainvalidaComponent } from './modals/modalfiltrodatainvalida/modalfiltrodatainvalida.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, // Ativa as Tags Angular no HTML
    ModalModule.forRoot(),
  ],
  declarations: [
    DataDirective,
    MesDirective,
    MascaraDirective,
    DatapickerComponent,
    ModalfiltrodatainvalidaComponent
  ],
  exports: [
    DataDirective,
    MesDirective,
    MascaraDirective,
    DatapickerComponent
  ],
  entryComponents: [
    ModalfiltrodatainvalidaComponent
  ],
  providers: [ DatePipe ]
})
export class DataModule { }
