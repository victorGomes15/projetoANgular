import { AfterViewInit, Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { SessionService } from '../../common/_service/session.service';
import { DateComponent } from './_models/date.model';

declare var jQuery: any;

@Directive({
    selector: '[appMascara]',
    exportAs: 'MascaraDirective'
})
export class MascaraDirective implements AfterViewInit, DateComponent {

    public $element;

    constructor(private el: ElementRef, private modalService: BsModalService, private sessionService: SessionService) {
        this.$element = jQuery(this.el.nativeElement);
    }

    @Input('appMascara')
    public appMascara: string;

    @Input('valor')
    public get valor() {
        return this.$element.val();
    }

    @Input()
    public transformationIn: (valor: any) => string = ((valor) => valor);
    @Input()
    public transformationOut: (valor: string) => any = ((valor) => valor);

    @Output()
    private valorChange = new EventEmitter<string>();

    @Output()
    private focusoutAfterChange = new EventEmitter<string>();

    @Input()
    options;

    public set valor(outValue) {
        const inValue = this.transformationIn(outValue);
        this.setValorComponente(inValue);
        this.valorChange.emit(outValue);
    }

    public setValorComponente(value) {
        this.$element.val(value);
    }

    ngAfterViewInit() {
        this.sessionService.waitOne('layout').subscribe( layout => {
            this.modalService.onShow.subscribe( next => layout.sendEvent('startModal') );
            this.modalService.onHide.subscribe( next => layout.sendEvent('endModal') );
        } );
        if (this.options) {
            this.$element.mask(this.appMascara, this.options);
        } else {
            this.$element.mask(this.appMascara);
        }
    }

    public modal(content) {
        this.modalService.show(content, { backdrop: 'static' });
    }

    @HostListener('focusout', ['$event'])
    onFocusout(event) {
        const outValue = this.transformationOut(this.valor);
        this.valor = outValue;
        this.focusoutAfterChange.emit(outValue);
    }
}
