import { AfterViewInit, Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { SessionService } from '../../common/_service/session.service';
import { DateUtils } from './date-utils';
import { MascaraDirective } from './mascara.directive';

@Directive({
    selector: '[appMes]',
    exportAs: 'MesDirective'
})
export class MesDirective extends MascaraDirective implements AfterViewInit {

    public _format = 'date';

    constructor(el: ElementRef, modalService: BsModalService, sessionService: SessionService) {
        super(el, modalService, sessionService);
        this.appMascara = '99/9999';
    }

    @Input()
    /** formato do objeto modelo */
    public get format() {
        return this._format;
    }

    public set format(value) {
        this._format = value;
        this.formatChange();
        this.valor = this.transformationOut(this.valor);
    }

    @Input('appMes')
    public get appMes() {
        return this.valor;
    }

    public set appMes(value) {
        this.valor = value;
    }

    @Output()
    private appMesChange = new EventEmitter<string>();

    public static datetimeInicioTransformationOut(value) {
        if (value) {
            return DateUtils.dateTimeTransformationOut('01/' + value, undefined, DateUtils.FIRST_INSTANT);
        }
        return value;
    }

    public static datetimeFimTransformationOut(value) {
        if (value && DateUtils.isValidMonth(value)) {
            const valorNumeros = value.replace(/[^0-9]/g, '');
            const mes = valorNumeros.substring(0, 2);
            const ano = valorNumeros.substring(2, 6);
            const date = new Date(parseInt(ano, 10), parseInt(mes, 10), 0);
            const data = date.getDate() + '/' + mes + '/' + ano;
            return DateUtils.dateTimeTransformationOut(data, undefined, DateUtils.LAST_INSTANT);
        }
        return undefined;
    }

    ngAfterViewInit() {
        super.ngAfterViewInit();
    }

    private formatChange() {
        if (this._format === 'date') {
            this.transformationIn = (value) => {
                const transformed = DateUtils.dateTransformationIn(value);
                if (transformed) {
                    return transformed.substring(3);
                }
                return transformed;
            };
            this.transformationOut = (value: string) => DateUtils.dateTransformationOut('01/' + value);
        } else if (this._format === 'datetimeInicio') {
            this.transformationIn = (value) => {
                const transformed = DateUtils.dateTransformationIn(value);
                if (transformed) {
                    return transformed.substring(3);
                }
                return transformed;
            };
            this.transformationOut = MesDirective.datetimeInicioTransformationOut;
        } else if (this._format === 'datetimeFim') {
            this.transformationIn = (value) => {
                const transformed = DateUtils.dateTransformationIn(value);
                if (transformed) {
                    return transformed.substring(3);
                }
                return transformed;
            };
            this.transformationOut = MesDirective.datetimeFimTransformationOut;
        }
    }

    @HostListener('focusoutAfterChange', ['$event'])
    focusoutAfterChangeEvent(event) {
        this.appMesChange.emit(event);
        DateUtils.componentMonthIsValid(this);
    }
}
