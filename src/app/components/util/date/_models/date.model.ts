export interface DatapickerOptions {
    minViewMode?: number;
    format?: string;
    language?: string;
    autoclose?: boolean;
    forceParse?: boolean;
}

export interface DateComponent {
    readonly valor: string;
    modal: (content) => any;
}
