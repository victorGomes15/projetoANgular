import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { IboxtoolsModule } from '../common/iboxtools/iboxtools.module';
import { StringLimitPipe } from '../common/pipes/string-limit';
import { DataTableComponent } from './data-table.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgxPaginationModule,
        IboxtoolsModule,
    ],
    declarations: [
        DataTableComponent,
        StringLimitPipe,
    ],
    exports: [ DataTableComponent ]
})
export class DataTableModule { }
