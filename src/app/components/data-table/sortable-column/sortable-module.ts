import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SortService } from './sort.service';
import { SortableColumnComponent } from './sortable-column.component';
import { SortableTableDirective } from './sortable-table.directive';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    declarations: [
        SortableColumnComponent,
        SortableTableDirective
    ],
    exports: [ SortableColumnComponent, SortableTableDirective ],
    providers: [ SortService ]
})
export class SortableModule { }
