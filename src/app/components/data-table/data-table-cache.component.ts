import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class DataTableCache {

    private _data: Map<string, DataCache> = new Map<string, DataCache>();
    private _carregandoMais: Map<string, EventEmitter<boolean>> = new Map<string, EventEmitter<boolean>>();

    constructor() { }

    public getCarregandoMaisInstance(id: string) {
        if (this._carregandoMais.get(id)) {
            return this._carregandoMais.get(id);
        } else {
            this._carregandoMais.set(id, new EventEmitter());
            return this._carregandoMais.get(id);
        }
    }

    public setValue(id: string, pagina: number, data: any[]): void {
        if (this._data.get(id)) {
            this._data.get(id).cache.set(pagina, data);
        } else {
            const dataCache = new DataCache();
            dataCache.cache.set(pagina, data);
            this._data.set(id, dataCache);
        }
    }

    public getValue(id: string): DataCache {
        return this._data.get(id);
    }

    public clearCache(id: string): void {
        if (this._data.get(id)) {
            this._data.set(id, null);
        }
    }

}

export class DataCache {
    cache: Map<number, any[]> = new Map<number, any[]>();
}
