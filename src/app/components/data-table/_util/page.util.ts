import { Paginacao, FiltroPage, FiltroMultiPage } from '../_models/paginacao.model';
import { DataTableConfig, PaginacaoConfig } from '../_models/data-table.model';

export class PageUtil {

    static PRIMEIRA_PAGINA = 0;

    public static pageChangeEvent(event: Paginacao, filtro: FiltroPage, config: DataTableConfig) {
        filtro.offset = event.offset();
        config.configuracaoPaginacao.currentPage = event.paginaAtual;
        filtro.size = event.quantidadePorPagina;
    }

    public static paginacaoEvent(event: number, filtro: FiltroPage, config: DataTableConfig) {
        filtro.size = event;
        config.configuracaoPaginacao.itemsPerPage = event;
        config.configuracaoPaginacao.currentPage = PaginacaoConfig.PRIMEIRA_PAGINA;
        filtro.offset = PageUtil.PRIMEIRA_PAGINA;
    }

    public static pageMultiChangeEvent(event: Paginacao, filtro: FiltroMultiPage, config: DataTableConfig, table: number) {
        filtro.offset[table] = event.offset();
        config.configuracaoPaginacao.currentPage = event.paginaAtual;
        filtro.size[table] = event.quantidadePorPagina;
    }

    public static paginacaoMultiEvent(event: number, filtro: FiltroMultiPage, config: DataTableConfig, table: number) {
        filtro.size[table] = event;
        config.configuracaoPaginacao.itemsPerPage = event;
        config.configuracaoPaginacao.currentPage = PaginacaoConfig.PRIMEIRA_PAGINA;
        filtro.offset[table] = PageUtil.PRIMEIRA_PAGINA;
    }
}