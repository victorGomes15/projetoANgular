import { AfterViewInit, Component, ContentChild, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { IboxToolsConfig, IDropdownMenuItem, IIboxToolsConfigParameter } from '../common/iboxtools/_models/iboxtools.model';
import { ButtonGroupEvent, ButtonInLineEvent, ClickEvent } from './_models/button.model';
import { DataTableConfig, ElementType, PaginacaoConfig, Listras } from './_models/data-table.model';
import { Paginacao, FiltroPage, FiltroMultiPage } from './_models/paginacao.model';
import { PageUtil } from './_util/page.util';
import { instanceOfListagemPage } from '../common/interfaces/listagem.interface';
declare var jQuery: any;

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit, AfterViewInit {

  @Input() cacheId: string;
  @Input() configDataTable: DataTableConfig;
  @Input() collapseOption = true;
  @Input() templates: Map<ElementType, any> = new Map();
  @Output() pageChangeEvent = new EventEmitter<Paginacao>();
  @Output() selecionadosEvent = new EventEmitter<ButtonGroupEvent>();
  @Output() inLineButtonsEvent = new EventEmitter<ButtonInLineEvent>();
  @Output() paginacaoEvent = new EventEmitter<number>();
  @Output() buttonSearchEvent = new EventEmitter<string>();
  @Output() clickEvent = new EventEmitter<ClickEvent>();
  @Output() selecionadosItensEvent = new EventEmitter<Array<any>>();

  isSmall = false;
  todosSelecionados = false;
  maxPage: number;
  config: PaginacaoConfig = null;
  paginationId = UUID.UUID();
  allPropertiesFromData;

  iboxToolsConfig: IIboxToolsConfigParameter = new IboxToolsConfig();

  constructor() { }

  @ViewChild('templateColunaLinkspanInside')
    templateColunaLinkspanInside;
  @ViewChild('templateColunaLinkspan')
    templateColunaLinkspan;
  @ViewChild('templateColunaLink')
    templateColunaLink;
  @ViewChild('templateColunaSpan')
    templateColunaSpan;
  @ViewChild('templateColunaImage')
    templateColunaImage;
  @ViewChild('templateColunaDefault')
    templateColunaDefault;
  @ViewChild('templateColunaMultiple')
    templateColunaMultiple;
  @ViewChild('templateHtmlSpan')
    templateHtmlSpan;

  @ViewChild('p')
    p;

  @ContentChild('dt_iboxtitle')
    dt_iboxtitle;
  @ContentChild('dt_iboxtools')
    dt_iboxtools;
  @ContentChild('dt_iboxcontent_pre')
    dt_iboxcontent_pre;
  @ContentChild('dt_iboxcontent_pos')
    dt_iboxcontent_pos;

  ngOnInit() {
    setTimeout(() => {
      jQuery('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
      });
    }, 0);
    this.config = this.configDataTable.configuracaoPaginacao;
    this.config.id = this.paginationId;
    this.configDataTable.dataObservable.subscribe( next => {
      this.configDataTable.data.forEach( item => item.collapse = this.configDataTable.useCollapseAttribute );
      this.setAllPropertiesFromData();
    } );
    if (this.configDataTable.data) {
      this.configDataTable.update();
    }
  }

  ngAfterViewInit() {
    this.templatesDefaultsInit();
    this.config.totalItemsChange.subscribe( value => this.parametersInit(value) );
    this.parametersInit(this.config.totalItems || 0);
  }

  private parametersInit(value: number) {
    let listItens = [];
    let retorno;
    if (this.configDataTable.paginacoes) {
      retorno = this.configDataTable.paginacoes;
    } else {
      const paginacoesPadrao = [5, 10, 25, 50];
      const bigger = paginacoesPadrao.find(item => item >= value) || value;
      retorno = paginacoesPadrao.filter(item => item <= bigger);
    }
    if (retorno.length > 1) {
      listItens = retorno.map(item => <IDropdownMenuItem>{
        acao: () => {
          this.todosSelecionados = false;
          this.configDataTable.selecionados = [];
          if (this.configDataTable.filtroPage) {
            if (this.isFiltroPage(this.configDataTable.filtroPage)) {
              PageUtil.paginacaoEvent(item, <FiltroPage>this.configDataTable.filtroPage, this.configDataTable);
            } else {
              PageUtil.paginacaoMultiEvent(item, <FiltroMultiPage>this.configDataTable.filtroPage,
                this.configDataTable, this.configDataTable.customId);
            }
            if (instanceOfListagemPage(this.configDataTable.componentListagem)) {
              this.configDataTable.componentListagem.btnFiltrar();
            }
          }
          this.paginacaoEvent.emit(item);
        },
        titulo: item + ' Itens por Página'
      });
    }
    this.iboxToolsConfig = {
      hasCollapse: () => this.collapseOption,
      dropdownMenu: listItens
    };
  }

  private templatesDefaultsInit() {
    this.templates.set(ElementType.DEFAULT, this.templateColunaDefault);
    this.templates.set(ElementType.IMAGE, this.templateColunaImage);
    this.templates.set(ElementType.LINK, this.templateColunaLink);
    this.templates.set(ElementType.LINK_SPAN, this.templateColunaLinkspan);
    this.templates.set(ElementType.LINK_SPAN_INSIDE, this.templateColunaLinkspanInside);
    this.templates.set(ElementType.SPAN, this.templateColunaSpan);
    this.templates.set(ElementType.MULTIPLE, this.templateColunaMultiple);
    this.templates.set(ElementType.HTML_SPAN, this.templateHtmlSpan);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth < 768) {
      this.isSmall = true;
    }

    if (window.innerWidth >= 1200) {
      this.isSmall = false;
    }
  }

  pageChange(paginaAtual: number): void {
      this.todosSelecionados = false;
      this.configDataTable.selecionados = [];
      const paginacao = new Paginacao(paginaAtual, this.config.itemsPerPage);
      if (this.configDataTable.filtroPage) {
        if (this.isFiltroPage(this.configDataTable.filtroPage)) {
          PageUtil.pageChangeEvent(paginacao, <FiltroPage>this.configDataTable.filtroPage, this.configDataTable);
        } else {
          PageUtil.pageMultiChangeEvent(paginacao, <FiltroMultiPage>this.configDataTable.filtroPage,
            this.configDataTable, this.configDataTable.customId);
        }
        if (instanceOfListagemPage(this.configDataTable.componentListagem)) {
          this.configDataTable.componentListagem.btnFiltrar();
        }
      }
      this.pageChangeEvent.emit(paginacao);
      this.changePage(paginaAtual);
  }

  private isFiltroPage(params: FiltroPage | FiltroMultiPage): boolean {
    return !('qtdDataTable' in params);
  }

  changePage(paginaAtual: number) {
    this.config.currentPage = paginaAtual;
  }

  itemCollapsed(item) {
    return item.collapsed ? 'fa-angle-down' : 'fa-angle-right';
  }

  selectItem(item) {
    item.selected = !item.selected;
    if (item.selected) {
      this.configDataTable.selecionados.push(item);
    } else {
      this.configDataTable.selecionados = this.configDataTable.selecionados.filter(o => o !== item);
    }
    this.selecionadosItensEvent.emit(this.configDataTable.selecionados);
  }

  collapse(item: any) {
    if (item.collapse) {
      item.collapsed = !item.collapsed;
    }
  }

  setAllPropertiesFromData() {
    if (this.configDataTable.data.length > 0) {
      const ret = Object.getOwnPropertyNames(this.configDataTable.data[0]);
      this.allPropertiesFromData = [];
      ret.forEach(element => {
        if ('collapse' !== element && 'inLineButtonConfig' !== element && 'selected' !== element) {
          if (!this.configDataTable.colunas.map(v => v.atributo).includes(element)) {
            this.allPropertiesFromData.push(element);
          }
        }
      });
    }
  }

  getAllPropertiesFromData(): string[] {
    return this.allPropertiesFromData;
  }

  handleButton(id, item) {
    this.inLineButtonsEvent.emit(new ButtonInLineEvent(id, item));
  }

  handleButtonSearch(item) {
    this.buttonSearchEvent.emit(item);
  }

  handleButtonGroup(id) {
    this.selecionadosEvent.emit(new ButtonGroupEvent(id, this.configDataTable.selecionados));
  }

  handleClickEvent(atributo: string, item) {
    this.clickEvent.emit(new ClickEvent(atributo, item));
  }

  selectAll() {
    const aux = this.configDataTable.data.slice(0, this.config.itemsPerPage);
    if (!this.todosSelecionados) {
      this.configDataTable.data.forEach(value => value.selected = true);
      this.configDataTable.selecionados = aux;
    } else {
      this.configDataTable.data.forEach(value => value.selected = false);
      this.configDataTable.selecionados = [];
    }
    this.todosSelecionados = !this.todosSelecionados;
    this.selecionadosItensEvent.emit(this.configDataTable.selecionados);
  }

  isAlgumElementoSelecionado() {
    return this.configDataTable.selecionados && this.configDataTable.selecionados.length !== 0;
  }

  calcularCurrentAndTotal(maxSize: number, currentPage: number) {
    const first = ((currentPage - 1) * this.configDataTable.configuracaoPaginacao.itemsPerPage) + 1;
    const until = (first + this.configDataTable.configuracaoPaginacao.itemsPerPage) - 1;
    const untilMin = Math.min(until, this.configDataTable.configuracaoPaginacao.totalItems);
    return ' (' + first + ' - ' + untilMin + ' / ' + this.configDataTable.configuracaoPaginacao.totalItems + ')';
  }

  public colTemplate(dat, col) {
    let template;
    if (dat.customConfig && dat.customConfig.get(col.atributo)) {
      const coluna = dat.customConfig.get(col.atributo);
      if (coluna.template) {
        template = coluna.template;
      } else if (coluna.tipoElemento) {
        template = this.templates.get(coluna.tipoElemento);
      }
    } else if (col.customConfig && col.customConfig.template) {
      template = col.customConfig.template;
    } else if (col.customConfig && col.customConfig.tipoElemento) {
      template = this.templates.get(col.customConfig.tipoElemento);
    } else if (col.linkTitle) {
      template = this.templates.get(ElementType.LINK);
    }
    return template || this.templates.get(ElementType.DEFAULT);
  }

  public colTemplateContext(dat, col) {
    let context;
    if (dat.customConfig && dat.customConfig.get(col.atributo)) {
      context = dat.customConfig.get(col.atributo).templateContext;
    } else if (col.customConfig && col.customConfig.templateContext) {
      context = col.customConfig.templateContext;
    }
    context = context || {};
    context.dat = dat;
    context.col = col;
    return context;
  }

  public iboxtoolsTemplateContext(configDataTable) {
    const context = {
      iboxToolsConfig: this.iboxToolsConfig,
      configDataTable: configDataTable,
      p: this.p
    };
    return context;
  }

  comListra(isOdd) {
    return (this.configDataTable.listras === Listras.IMPAR && isOdd) || (this.configDataTable.listras === Listras.PAR && !isOdd);
  }
}
