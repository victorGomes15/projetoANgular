export class ButtonInLineWrapper {
    titulo: string;
    botoes: ButtonInLine[];
    tdClass = 'col-md-3 col-sm-3 col-lg-3 text-center';
    divClass: string;
    headerClass = 'text-center';
    constructor(botoes?: ButtonInLine[]) {
        this.botoes = botoes;
    }
}

export class ButtonInLine {
    htmlClasses: string;
    title: string;
    descricao: string;
    faClass: string;
    id: string;
    style: any = { 'margin-left': '2px'};

    constructor(builder?) {
        if (builder) {
            this.htmlClasses = builder.htmlClasses;
            this.title = builder.title;
            this.descricao = builder.descricao;
            this.faClass = builder.faClass;
            this.id = builder.id;
            this.style = builder.style;
        }
    }

    static get Builder() {
        class Builder {
            private htmlClasses: string;
            private title: string;
            private descricao: string;
            private faClass: string;
            private id: string;
            private style: any = { 'margin-left': '2px'};

            constructor(id: string) {
                this.id = id;
            }

            withHtmlClasses(htmlClasses: string): Builder {
                this.htmlClasses = htmlClasses;
                return this;
            }

            withTitle(title: string): Builder {
                this.title = title;
                return this;
            }

            withDescricao(descricao: string): Builder {
                this.descricao = descricao;
                return this;
            }

            withFaClass(faClass: string): Builder {
                this.faClass = faClass;
                return this;
            }

            withStyle(style: any): Builder {
                this.style = style;
                return this;
            }

            noStyle(): Builder {
                this.style = {};
                return this;
            }

            build(): ButtonInLine {
                return new ButtonInLine(this);
            }

        }

        return Builder;
    }
}

export class ButtonSearch {
    htmlClasses: string;
    title: string;
    descricao: string;
    faClass: string;
    id: string;
    searchValue: string;
}

export class ButtonInLineEvent {
    constructor(public id: string, public item: any) { }
}

export class ButtonGroup {
    htmlClasses: string;
    descricao: string;
    faClass: string;
    id: string;
    title: string;
}

export class ButtonGroupEvent {
    constructor(public id: string, public items: any[]) { }
}

export class ExportButtonEvent {
    constructor(public id: string) { }
}
export class ClickEvent {
    constructor(public atributo: string, public item: any) { }
}
