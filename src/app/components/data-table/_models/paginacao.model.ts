export class Paginacao {
    constructor(public paginaAtual: number, public quantidadePorPagina: number) {
    }

    // Retornar o numero real utilizado para paginação no serviço
    public offset(): number {
        if (this.paginaAtual > 0) {
            return this.paginaAtual - 1;
        } else {
            return this.paginaAtual;
        }
    }
}

export class Pageable {

    public offset: number;
    public maxOffset: number;
    public total: number;

    get page(): number {
        if (this.offset >= 0) {
            return this.offset + 1;
        } else {
            return this.offset;
        }
    }

    get maxPage(): number {
        if (this.maxOffset >= 0) {
            return this.maxOffset + 1;
        } else {
            return this.maxOffset;
        }
    }
}

export interface FiltroPage {
    offset: number;
    size: number;
}

export interface FiltroMultiPage {
    offset: Array<number>;
    size: Array<number>;
    qtdDataTable: number;
}
