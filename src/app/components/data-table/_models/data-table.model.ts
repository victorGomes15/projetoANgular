import { EventEmitter } from '@angular/core';
import { Observable, Subject } from 'rxjs/';
import { Messages } from '../../common/messages/messages';
import { ButtonGroup, ButtonInLineWrapper, ButtonSearch } from './button.model';
import { FiltroMultiPage, FiltroPage } from './paginacao.model';
import { ListagemPage } from '../../common/interfaces/listagem.interface';

export class DataTableColumn {
    constructor(
        public atributo: string,
        public apelido?: string,
        linkTitle: boolean = false,
        public customConfig = {}
    ) {
        if (linkTitle) {
            this.customConfig['tipoElemento'] = ElementType.LINK;
        }
        customConfig = Object.assign({ stringLimit: 30 }, customConfig);
        // if (!apelido) {
        //     this.apelido = atributo;
        // }
    }
}

export class DataTable {
    public collapse = false;
    public selected = false;
    public inLineButtonConfig: Map<string, boolean>;
    public inLineButtonEnableConfig: Map<string, boolean>;
    public customConfig: Map<string, InLineConfig>;
}

export enum ElementType {
    SPAN = 'span',
    LINK = 'link',
    LINK_SPAN = 'link_span',
    LINK_SPAN_INSIDE = 'link_span_inside',
    MULTIPLE = 'multiple',
    IMAGE = 'image',
    DEFAULT = 'default',
    HTML_SPAN = 'html_span'
}

export class InLineConfig {
    // Não achei jeito melhor de fazer isso
    public srcImage = '';
    public spanClass = '';
    public faClass = '';
    public spanTitle = '';
    public titleImage = '';
    public tipoElemento: ElementType = ElementType.DEFAULT;
}

export enum PaginacaoAlignment {
    ESQUERDA = 'text-center m-t-lg pull-left',
    CENTRO = 'text-center m-t-lg',
    DIREITA = 'text-center m-t-lg pull-right'
}

export class PaginacaoConfig {

    static PRIMEIRA_PAGINA = 1;

    public id;
    public itemsPerPage = 10;
    public currentPage = PaginacaoConfig.PRIMEIRA_PAGINA;
    private _totalItems: number;
    public get totalItems(): number {
        return this._totalItems;
    }
    public set totalItems(value: number) {
        this._totalItems = value;
        this.totalItemsChange.emit(value);
    }
    public totalItemsChange = new EventEmitter<number>();
    public alignment: PaginacaoAlignment = PaginacaoAlignment.CENTRO;
    public useCustomPagination = true;
    public showTotal = false;
    public autoHide = false;
}

export class CustomEmptyResult {
    public class = 'alert alert-success';
    public message = Messages.MENSAGEM_EMPTY_RESULT_USUARIO_ACESSO_CUSTOMIZADO;
}

export enum Listras {
    IMPAR,
    PAR,
    NONE
}

export class DataTableConfig {
    private _inLineButtons: ButtonInLineWrapper = undefined;
    private _buttonSearch: ButtonSearch = undefined;
    private _groupButtons: ButtonGroup[] = [];
    private _title: string;
    private _useSelectAttribute = true;
    private _useSearchAttribute = false;
    private _useCollapseAttribute = true;
    private _useExportButtonPdf = true;
    private _useExportButtonXLS = true;
    private _showGroupButtons = false;
    private _showEmpty = false;
    private _showInLineButtonsAlways = false;
    private _paginacoes: number[];
    private _configuracaoPaginacao = new PaginacaoConfig();
    private _dataChange = new Subject<any>();
    private _paginacaoOption = true;
    private _useExportButtonCollapsed = false;
    private _listras = Listras.NONE;
    private _frame = true;
    private _dataAll = [];
    private _alwaysShowButtonReport = false;
    private _selecionados = [];
    private _customEmptyResult: CustomEmptyResult = undefined;
    private _customEmptyResultTemplate = undefined;
    private _filtroPage: FiltroPage | FiltroMultiPage = undefined;
    private _customId: number = undefined;
    private _componentListagem: ListagemPage = undefined;

    private _subject: Subject<any> = new Subject();

    constructor(private _data?: DataTable[], private _colunas?: DataTableColumn[]) {
        this.data = _data;
    }

    public get data(): DataTable[] {
        return this._data;
    }

    public set data(value: DataTable[]) {
        this._data = value || [];
    }

    public update() {
        this._dataChange.next(this._data);
    }

    public get dataObservable(): Observable<DataTable[]> {
        return this._dataChange.asObservable();
    }

    public get colunas(): DataTableColumn[] {
        return this._colunas;
    }

    public set colunas(value: DataTableColumn[]) {
        this._colunas = value;
    }

    public get inLineButtons(): ButtonInLineWrapper {
        return this._inLineButtons;
    }

    public set inLineButtons(value: ButtonInLineWrapper) {
        this._inLineButtons = value;
    }

    public get buttonSearch(): ButtonSearch {
        return this._buttonSearch;
    }

    public set buttonSearch(value: ButtonSearch) {
        this._buttonSearch = value;
    }

    public get groupButtons(): ButtonGroup[] {
        return this._groupButtons;
    }

    public set groupButtons(value: ButtonGroup[]) {
        this._groupButtons = value;
    }

    public get title(): string {
        return this._title;
    }

    public set title(value: string) {
        this._title = value;
    }

    public get useSelectAttribute(): boolean {
        return this._useSelectAttribute;
    }

    public set useSelectAttribute(value: boolean) {
        this._useSelectAttribute = value;
    }

    public get useSearchAttribute(): boolean {
        return this._useSearchAttribute;
    }

    public set useSearchAttribute(value: boolean) {
        this._useSearchAttribute = value;
    }

    public get useCollapseAttribute(): boolean {
        return this._useCollapseAttribute;
    }
    public set useCollapseAttribute(value: boolean) {
        this._useCollapseAttribute = value;
    }

    public get useExportButtonPdf(): boolean {
        return this._useExportButtonPdf;
    }

    public set useExportButtonPdf(value: boolean) {
        this._useExportButtonPdf = value;
    }

    public get useExportButtonXLS(): boolean {
        return this._useExportButtonXLS;
    }

    public set useExportButtonXLS(value: boolean) {
        this._useExportButtonXLS = value;
    }

    public get paginacoes(): number[] {
        return this._paginacoes;
    }

    public set paginacoes(value: number[]) {
        this._paginacoes = value;
    }

    public get configuracaoPaginacao(): PaginacaoConfig {
        return this._configuracaoPaginacao;
    }

    public set configuracaoPaginacao(value: PaginacaoConfig) {
        this._configuracaoPaginacao = value;
    }

    public get showGroupButtons(): boolean {
        return this._showGroupButtons;
    }

    public set showGroupButtons(value: boolean) {
        this._showGroupButtons = value;
    }

    public get showEmpty(): boolean {
        return this._showEmpty;
    }

    public set showEmpty(value: boolean) {
        this._showEmpty = value;
    }

    public get showInLineButtonsAlways(): boolean {
        return this._showInLineButtonsAlways;
    }

    public set showInLineButtonsAlways(value: boolean) {
        this._showInLineButtonsAlways = value;
    }

    public get paginacaoOption(): boolean {
        return this._paginacaoOption;
    }

    public set paginacaoOption(value: boolean) {
        this._paginacaoOption = value;
    }

    public get useExportButtonCollapsed(): boolean {
        return this._useExportButtonCollapsed;
    }

    public set useExportButtonCollapsed(value: boolean) {
        this._useExportButtonCollapsed = value;
    }

    public get listras(): Listras {
        return this._listras;
    }

    public set listras(value: Listras) {
        this._listras = value;
    }

    public get frame(): boolean {
        return this._frame;
    }

    public set frame(value: boolean) {
        this._frame = value;
    }

    public get dataAll(): DataTable[] {
        return this._dataAll;
    }

    public set dataAll(value: DataTable[]) {
        this._dataAll = value || [];
        this._subject.next(this._dataAll);
    }

    public get subject() {
        return this._subject;
    }

    public get alwaysShowButtonReport(): boolean {
        return this._alwaysShowButtonReport;
    }

    public set alwaysShowButtonReport(value: boolean) {
        this._alwaysShowButtonReport = value;
    }

    public get selecionados(): Array<any> {
        return this._selecionados;
    }

    public set selecionados(selecionados: Array<any>) {
        this._selecionados = selecionados;
    }

    public reseteSelecionados() {
        this._selecionados = [];
    }

    public get customEmptyResult(): CustomEmptyResult {
        return this._customEmptyResult;
    }

    public set customEmptyResult(value: CustomEmptyResult) {
        this._customEmptyResult = value;
    }

    public get customEmptyResultTemplate() {
        return this._customEmptyResultTemplate;
    }

    public set customEmptyResultTemplate(value) {
        this._customEmptyResultTemplate = value;
    }

    public get filtroPage(): FiltroPage | FiltroMultiPage {
        return this._filtroPage;
    }

    public set filtroPage(value: FiltroPage | FiltroMultiPage) {
        this._filtroPage = value;
    }

    public get customId(): number {
        return this._customId;
    }

    public set customId(value: number) {
        this._customId = value;
    }

    public get componentListagem(): ListagemPage {
        return this._componentListagem;
    }

    public set componentListagem(value: ListagemPage) {
        this._componentListagem = value;
    }


}
