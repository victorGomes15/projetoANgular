import { DataTable } from '../data-table/_models/data-table.model';

export class AutorizacaoSistemaTypeResponse {

    usuario?: UsuarioType;
    autorizacaoSistemas?: AutorizacaoSistemaType[];
    content?: AutorizacaoSistemaType[];

}

export class AutorizacaoSistemaType {

    permissoes: PermissaoType[];

}

export class PermissaoType {

    permissaoId: string;
    negar: boolean;

}

export class UsuarioTypeResponse {

    private _content: UsuarioType[];

    public get content(): UsuarioType[] {
        return this._content;
    }

    public set content(value: UsuarioType[]) {
        this._content = value;
    }

}

export class Teste extends DataTable {
    public campoUm: string;
    public campoDois: number;
}

export class UsuarioType extends DataTable {

    private _responsavelFinanceiro: boolean;
    private _dataAlteracao: Date;
    private _grupo: string;
    private _emailPrincipal: string;
    private _master: boolean;
    private _senha: string;
    private _dataExpiracaoConta: Date; // Sem hora
    private _usuarioId: string;
    private _tipo: string;
    private _situacao: string;
    private _migrando: boolean;
    private _bloqueado: boolean;
    private _dataExpiracaoCredencial: Date;
    private _nomeFantasia: string;
    private _identidade: Identidade;

    constructor() {
        super();
    }

    public get responsavelFinanceiro(): boolean {
        return this._responsavelFinanceiro;
    }

    public set responsavelFinanceiro(value: boolean) {
        this._responsavelFinanceiro = value;
    }

    public get dataAlteracao(): Date {
        return this._dataAlteracao;
    }

    public set dataAlteracao(value: Date) {
        this._dataAlteracao = value;
    }

    public get grupo(): string {
        return this._grupo;
    }

    public set grupo(value: string) {
        this._grupo = value;
    }

    public get master(): boolean {
        return this._master;
    }

    public set master(value: boolean) {
        this._master = value;
    }

    public get emailPrincipal(): string {
        return this._emailPrincipal;
    }

    public set emailPrincipal(value: string) {
        this._emailPrincipal = value;
    }

    public get senha(): string {
        return this._senha;
    }

    public set senha(value: string) {
        this._senha = value;
    }

    public get dataExpiracaoConta(): Date {
        return this._dataExpiracaoConta;
    }

    public set dataExpiracaoConta(value: Date) {
        this._dataExpiracaoConta = value;
    }

    public get usuarioId(): string {
        return this._usuarioId;
    }

    public set usuarioId(value: string) {
        this._usuarioId = value;
    }

    public get tipo(): string {
        return this._tipo;
    }

    public set tipo(value: string) {
        this._tipo = value;
    }

    public get situacao(): string {
        return this._situacao;
    }

    public set situacao(value: string) {
        this._situacao = value;
    }

    public get migrando(): boolean {
        return this._migrando;
    }

    public set migrando(value: boolean) {
        this._migrando = value;
    }

    public get bloqueado(): boolean {
        return this._bloqueado;
    }

    public set bloqueado(value: boolean) {
        this._bloqueado = value;
    }

    public get dataExpiracaoCredencial(): Date {
        return this._dataExpiracaoCredencial;
    }

    public set dataExpiracaoCredencial(value: Date) {
        this._dataExpiracaoCredencial = value;
    }

    public get nomeFantasia(): string {
        return this._nomeFantasia;
    }

    public set nomeFantasia(value: string) {
        this._nomeFantasia = value;
    }

    public get identidade(): Identidade {
        return this._identidade;
    }

    public set identidade(value: Identidade) {
        this._identidade = value;
    }

}

export class Identidade {
    private _clienteId: number;

    public get clienteId(): number {
        return this._clienteId;
    }

    public set clienteId(value: number) {
        this._clienteId = value;
    }
}

export class IdentificacaoUsuario extends Identidade {
    usuarioSemApelido: string;

    constructor(
        item?: {
            clienteId?: number, usuarioSemApelido?: string
        },
    ) {
        super();
        if (item) {
            Object.assign(this, item);
        }
    }
}
