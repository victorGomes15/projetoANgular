import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Base64 } from 'js-base64';
import { Cookie } from 'ng2-cookies';
import { Observable, Subject } from 'rxjs/';
import { concat, take } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import * as oauth2 from '../../security/oauth2/oauth2.constants';
import { HeaderUtils } from '../util/headers/header-utils';
import { AutorizacaoSistemaTypeResponse, IdentificacaoUsuario, UsuarioType, UsuarioTypeResponse } from './usuario.model';
import { ContadorResource } from '../../_model/contador';

@Injectable()
export class ConnectContService {

    static readonly UNAUTHORIZED_HEADERS = new HttpHeaders({
        'Accept': 'application/json',
    });

    readonly url_sso: string = environment.urlSSO;
    private readonly urlClientes = this.url_sso + '/clientes/self';
    readonly access_token: string = 'access_token';
    private _usuario: UsuarioType = null;

    constructor(private httpClient: HttpClient) {
    }

    getHeaders(): HttpHeaders {
        return new HttpHeaders({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + Cookie.get(oauth2.accessToken)
        });
    }

    // GET /usuarios/{usuarioId}
    getUsuario(): Observable<any> {
        const options = { headers: this.getHeaders() };
        return this.httpClient.get<UsuarioType>(environment.urlSSO + '/api/users/self', options);
    }

    mesmoUsuario(identificacao: IdentificacaoUsuario, outro: IdentificacaoUsuario) {
        return identificacao.clienteId === outro.clienteId
            && identificacao.usuarioSemApelido === outro.usuarioSemApelido;
    }

    usuarioAtual(... identificacoes: IdentificacaoUsuario[]): Observable<any> {
        return this.getUsuario();
    }

    extraiRazaoSocial(usuario: UsuarioType): string {
        if (usuario['pessoaFisicaEquiparada']) {
            return usuario['pessoaFisicaEquiparada'].nome;
        } else if (usuario['pessoaFisica']) {
            return usuario['pessoaFisica'].nome;
        } else {
            return usuario['pessoaJuridica'].razaoSocial;
        }
    }

    // GET /usuarios
    getUsuarios(params?: HttpParams): Observable<UsuarioTypeResponse> {
        const options = { headers: this.getHeaders(), params: params };
        return this.httpClient.get<UsuarioTypeResponse>(environment.urlSSO + '/usuarios', options);
    }

    isMasterUser(params?: HttpParams, detalhesObs = this.getDetalhes(params)): Observable<boolean> {
        return this.hasAnyRoles(['MASTER', 'ROLE_ADMIN'], params, detalhesObs);
    }

    hasAnyRoles(roles, params?: HttpParams, detalhesObs = this.getDetalhes(params)): Observable<boolean> {
        return this.getPermissoes(params, detalhesObs).map(permissao => roles.includes(permissao)).filter(permitido => permitido)
        .pipe(
            concat(Observable.of(false)),
            take(1)
        );
    }

    // GET /usuarios/{usuarioId}/detalhes
    getDetalhes(params?: HttpParams): Observable<AutorizacaoSistemaTypeResponse> {
        let dataObs = new Subject();
        const usuarioEncodado = localStorage.getItem('usuario');
        if (usuarioEncodado) {
            const usuarioDecodado = JSON.parse(Base64.decode(usuarioEncodado));
            return Observable.of(usuarioDecodado);
        } else {
            if (typeof params === 'undefined') {
                params = new HttpParams({
                    fromObject: { sistemaIds: oauth2.clientId_8 }
                });
            }
            const options = { headers: this.getHeaders(), params: params };
            this.httpClient.get<AutorizacaoSistemaTypeResponse>(environment.urlSSO + '/usuarios/self/detalhes', options)
                .subscribe(data => {
                    const usuario = Base64.encode(JSON.stringify(data));
                    localStorage.setItem('usuario', usuario);
                    dataObs.next(data);
                }, error => {
                    dataObs.error(error);
                    dataObs = new Subject();
                });
        }
        return dataObs;
    }

    getPermissoes(params?: HttpParams, detalhesObs = this.getDetalhes(params)): Observable<string> {
        return detalhesObs.switchMap(detalhes => {
            if (detalhes.usuario && detalhes.usuario.master) {
                return Observable.of('MASTER');
            }
            return Observable.of(detalhes)
                .filter(data1 => !!data1.autorizacaoSistemas)
                .switchMap(data2 => data2.autorizacaoSistemas)
                .filter(content => !!content.permissoes)
                .switchMap(content => content.permissoes)
                .filter(permissao => !permissao.negar)
                .map(permissao => permissao.permissaoId);
        });
    }

    // POST /usuarios/emails/senha
    trocaSenha(params?: HttpParams): Observable<any> {
        const options = { 'headers': ConnectContService.UNAUTHORIZED_HEADERS, 'params': params };
        return this.httpClient.post(environment.urlSSO + '/usuarios/emails/senha', {}, options);
    }

    // POST /usuarios/emails/login
    enviaLoginEmail(params?: HttpParams): Observable<any> {
        const options = { 'headers': ConnectContService.UNAUTHORIZED_HEADERS, 'params': params };
        return this.httpClient.post(environment.urlSSO + '/usuarios/emails/login', {}, options);
    }

    // PUT /usuarios/{usuarioId}/senha
    alterarSenha(body: { 'senhaAtual': string, 'senhaNova': string }): Observable<any> {
        const headers = this.getHeaders();
        const options = { 'headers': headers };
        return this.httpClient.put(environment.urlSSO + '/usuarios/self/senhas', body, options);
    }

    public getEmailCliente(): Observable<{ email: string }> {
        const dadosRodape = { email: '' };
        const options = { headers: this.getHeaders() };
        return this.httpClient.get(this.urlClientes, options)
            .flatMap((data: any) => data.produtos)
            .filter((item: any) => item.produtoId === parseInt(environment.clientId, 10))
            .map((produto: any) => {
                dadosRodape.email = produto.email;
                return dadosRodape;
            });
    }

    public getCliente(): Observable<ContadorResource> {
        return this.httpClient.get<ContadorResource>(this.urlClientes, {
        headers: HeaderUtils.getHeaderWithAuthorization()});
    }

}
