import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { OAuth2Service } from '../../security/oauth2/oauth2.service';
import { BasicLayoutComponent } from '../common/layouts/basicLayout.component';
import { SessionService } from '../common/_service/session.service';
import { Cookie } from 'ng2-cookies';

declare var jQuery: any;

@Injectable()
export class FullModeGuard implements CanActivate {

  // private urlOrigin: URL = new URL(window.location.href);

  constructor(
    private router: Router,
    private _service: OAuth2Service,
    public sessionService: SessionService,
  ) { }

  async canActivate(route: ActivatedRouteSnapshot) {
    let activate = true;
    if (route.queryParamMap.has('token') && route.queryParamMap.has('refresh')) {
      const token: string = route.queryParamMap.get('token');
      const refresh: string = route.queryParamMap.get('refresh');
      const expire: string = route.queryParamMap.get('expire');
      const clientId: string = route.queryParamMap.get('clienteId');
      const sessionLogin: string = route.queryParamMap.get('sessionLogin');

      await this._service.validarClientId(token, clientId).toPromise().then(
        data => {
          this._service.saveTokenDataInCookieContadorOnline(token, refresh, expire, sessionLogin);
        }, error => {
          activate = false;
          this.router.navigate(['/login', 'session']);
        });
    }
    if (!route.queryParamMap.has('mode') || route.queryParamMap.get('mode') !== 'clean') {
      route.routeConfig.component = BasicLayoutComponent;
    } else if (activate) {
      await this.sessionService.waitOne('layout').subscribe(layout => {
        if (layout.observable) {
          layout.observable.subscribe(next => {
            activate = false;
            layout.sendRedirect({ caminho: (<RouterStateSnapshot>route['_routerState']).url });
          });
        }
      });
    }
    return activate;
  }



}
