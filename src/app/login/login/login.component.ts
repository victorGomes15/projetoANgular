import { Component, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { LoginService } from '../login.service';
import { AlertService } from '../../components/alert/_services';
import { environment } from '../../../environments/environment';

@Component({
  moduleId: module.id, // Obrigatorio se for trabalhar com templateUrl ou para referenciar qualquer arquivo externo
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements AfterViewInit {

  constructor(
    public service: LoginService,
    private alert: AlertService,
    private route: ActivatedRoute,
  ) { }

  login(): void {
    this.service.obtainAccessToken();
  }

  version(): string {
    return `${environment.versions.version}.${environment.versions.revision} ${environment.versions.date}`;
  }

  ngAfterViewInit(): void {
    this.route.params.forEach((params: Params) => {
      const id: string = params['id'];
      if (id === 'session') {
        setTimeout(() => { this.alert.info('Sessão expirada.'); }, 0);
      } else if (id === 'notFound') {
        setTimeout(() => { this.alert.info('Domínio não encontrado.'); }, 0);
      } else if (id === 'siteDesabilitado') {
        setTimeout(() => { this.alert.info('Site desabilitado.'); }, 0);
      }
    });
  }

}
