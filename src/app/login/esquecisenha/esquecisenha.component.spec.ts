import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsquecisenhaComponent } from './esquecisenha.component';
import { EsquecisenhaService } from './esquecisenha.service';
import { Observable } from 'rxjs/Rx';
import { EsquecisenhaServiceTestData } from './esquecisenha.service.spec';
import { APP_BASE_HREF } from '@angular/common';
import { AppModule } from '../../app.module';

describe('EsquecisenhaComponent', () => {
  let component: EsquecisenhaComponent;
  let fixture: ComponentFixture<EsquecisenhaComponent>;
  let service: EsquecisenhaService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AppModule ],
      providers: [{provide: APP_BASE_HREF, useValue: '/'}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsquecisenhaComponent);
    component = fixture.componentInstance;
    service = fixture.debugElement.injector.get(EsquecisenhaService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // let preparaDadosTesteRetornoSucessoRecuperarSenha = () => {
  //   component.loginData.username  = 'raymond@progteste';
  //   component.acao = component.staticAbas.ABA_RECUPERAR_SENHA;
  // };

  // let preparaDadosTesteRetornoSucessoRecuperarConta = () => {
  //   component.loginData.username  = '';
  //   component.loginData.cpf       = '';
  //   component.acao = component.staticAbas.ABA_RECUPERAR_CONTA;
  // };

  // let testeRetornoSucesso = (idModal) => {
  //   component.recuperar();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(component.modalSenha.isShown()).toBeTruthy();
  //   let texto = '<span class="loginscreenRed">' + EsquecisenhaServiceTestData.RETORNO_SEM_COPIA[0] + '</span>';
  //   expect(modalSenha.textContent).toContain(texto);
  // };

  // it('teste integrado retorno sucesso recuperarSenha', () => {
  //   preparaDadosTesteRetornoSucessoRecuperarSenha();
  //   testeRetornoSucesso('#modalSenha');
  // });
  // it('teste unitario retorno sucesso recuperarSenha', () => {
  //   let spy = spyOn(service, 'recuperarSenha').and.returnValue(Observable.of(EsquecisenhaServiceTestData.RETORNO_SEM_COPIA));
  //   preparaDadosTesteRetornoSucessoRecuperarSenha();
  //   testeRetornoSucesso('#modalSenha');
  // });
  // it('teste integrado retorno sucesso recuperarConta', () => {
  //   preparaDadosTesteRetornoSucessoRecuperarConta();
  //   testeRetornoSucesso('#modalConta');
  // });
  // it('teste unitario retorno sucesso recuperarConta', () => {
  //   let spy = spyOn(service, 'recuperarConta').and.returnValue(Observable.of(EsquecisenhaServiceTestData.RETORNO_SEM_COPIA));
  //   preparaDadosTesteRetornoSucessoRecuperarConta();
  //   testeRetornoSucesso('#modalConta');
  // });
});
