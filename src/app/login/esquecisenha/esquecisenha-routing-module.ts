// https://angular.io/guide/router#refactor-the-routing-configuration-into-a-routing-module
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EsquecisenhaComponent } from './esquecisenha.component';

const esquecisenhaRoutes: Routes = [
    { path: 'esquecisenha', component: EsquecisenhaComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(
            esquecisenhaRoutes
        )
    ],
    exports: [
        RouterModule
    ]
})

export class EsquecisenhaRoutingModule { }
