import { TestBed, inject, async } from '@angular/core/testing';

import { EsquecisenhaService } from './esquecisenha.service';
import { Observable } from 'rxjs/Rx';
import { APP_BASE_HREF } from '@angular/common';
import { AppModule } from '../../app.module';

export class EsquecisenhaServiceTestData {

  public static readonly DADOS_ENVIO_ESQUECI_SENHA = {
    login           : '',
    enviarCopia     : false
  }
  public static readonly DADOS_ENVIO_ESQUECI_CONTA = {
    codigoOuApelido : '',
    cpf             : '',
    enviarCopia     : false
  }

  public static readonly RETORNO_SEM_COPIA = ['']
}

describe('EsquecisenhaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ AppModule ],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'},
        EsquecisenhaService
      ]
    });
  });

  it('should be created', inject([EsquecisenhaService], (service: EsquecisenhaService) => {
    expect(service).toBeTruthy();
  }));
  // it('teste integrado retorno sucesso recuperarSenha', async(() => inject([EsquecisenhaService], (service: EsquecisenhaService) => {
  //   service.recuperarSenha(EsquecisenhaServiceTestData.DADOS_ENVIO_ESQUECI_SENHA).subscribe(
  //     data => expect(data).toEqual(EsquecisenhaServiceTestData.RETORNO_SEM_COPIA)
  //   );
  // })));
  // it('teste integrado retorno sucesso recuperarConta', async(() => inject([EsquecisenhaService], (service: EsquecisenhaService) => {
  //   service.recuperarConta(EsquecisenhaServiceTestData.DADOS_ENVIO_ESQUECI_CONTA).subscribe(
  //     data => expect(data).toEqual(EsquecisenhaServiceTestData.RETORNO_SEM_COPIA)
  //   );
  // })));
});
