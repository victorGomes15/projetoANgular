import { Component, OnInit, ViewEncapsulation, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { EsquecisenhaService } from './esquecisenha.service';
import { ModalDirective } from 'ngx-bootstrap';
import { ValidationService } from '../../components/common/_service/validation.service';
import { ValidationSelector, Mensagens } from '../_model/validation.model';

@Component({
  moduleId: module.id, // Obrigatorio se for trabalhar com templateUrl ou para referenciar qualquer arquivo externo
  selector: 'app-esquecisenha',
  templateUrl: './esquecisenha.component.html',
  styleUrls: ['./esquecisenha.component.scss'],
  providers: [
    EsquecisenhaService,
  ],
  encapsulation: ViewEncapsulation.None,
})
export class EsquecisenhaComponent implements OnInit, AfterViewInit {

  private static readonly ABAS = {
    ABA_RECUPERAR_SENHA: 'recuperarSenha',
    ABA_RECUPERAR_CONTA: 'recuperarConta',
  };

  @ViewChild('modalSenha')
  private modalSenha: ModalDirective;
  @ViewChild('modalConta')
  private modalConta: ModalDirective;
  public acao: string = EsquecisenhaComponent.ABAS.ABA_RECUPERAR_SENHA;
  public emails = '';
  cpf;
  validations: ValidationSelector = new ValidationSelector();

  public loginData = {
    username: '',
    codigo: '',
    password: '',
    cpf: '',
    hasCopiaResponsavel: false
  };

  @ViewChild('formulario')
  private formulario;

  constructor(
    private _service: EsquecisenhaService,
    private _router: Router,
    public validationService: ValidationService,
  ) {
  }

  ngOnInit() {
    this.validationService.addFrom(this.validations, Mensagens.TELAS.TELA_ESQUECI);
  }

  ngAfterViewInit() {
    let origin = this.validationService.validationFor(this.validations, { components: 'username', condicoes: 'required' });
    this.validationService.addValidCallback(
      origin,
      () => {
        return !(
          this.acao === EsquecisenhaComponent.ABAS.ABA_RECUPERAR_SENHA &&
          this.formulario.controls['username'] &&
          this.formulario.controls['username'].errors &&
          this.formulario.controls['username'].errors.required
        );
      }
    );
    origin = this.validationService.validationFor(this.validations, { components: 'codigo', condicoes: 'required' });
    this.validationService.addValidCallback(
      origin,
      () => {
        return !(
          this.acao === EsquecisenhaComponent.ABAS.ABA_RECUPERAR_CONTA &&
          this.formulario.controls['codigo'] &&
          this.formulario.controls['codigo'].errors &&
          this.formulario.controls['codigo'].errors.required
        );
      }
    );
    origin = this.validationService.validationFor(this.validations, { components: 'cpf', condicoes: 'required' });
    this.validationService.addValidCallback(
      origin,
      () => {
        return !(
          this.acao === EsquecisenhaComponent.ABAS.ABA_RECUPERAR_CONTA &&
          this.formulario.controls['cpf'] &&
          this.formulario.controls['cpf'].errors &&
          this.formulario.controls['cpf'].errors.required
        );
      }
    );
  }

  public voltar(): void {
    this._router.navigate(['/'], { queryParamsHandling: 'merge'});
  }

  public recuperar(): void {
    if (this.validationService.isValid(this.validations)) {
      const copia = this.loginData.hasCopiaResponsavel;
      if (EsquecisenhaComponent.ABAS.ABA_RECUPERAR_SENHA === this.acao) {
        this._service.recuperarSenha({
          login: this.loginData.username,
          enviarCopia: this.loginData.hasCopiaResponsavel
        }).subscribe(data => this.recuperarSenhaSucesso(data, copia));
      } else if (EsquecisenhaComponent.ABAS.ABA_RECUPERAR_CONTA === this.acao) {
        const data = this._service.recuperarConta({
          codigoOuApelido: this.loginData.codigo,
          cpf: this.loginData.cpf,
          enviarCopia: this.loginData.hasCopiaResponsavel
        }).subscribe(data1 => this.recuperarContaSucesso(data1, copia));
      }
    } else {
      this.validationService.showMessages(this.validations);
    }
  }

  private preencheEmail(data, copia): string {
    // this.emails = data.join(", ");
    let thisEmails = '<span class="loginscreenRed">' + data[0] + '</span>';
    if (copia) {
      thisEmails += ', com cópia para ';
      thisEmails += '<span class="loginscreenRed">' + data[1] + '</span>';
    }
    return thisEmails;
  }

  cpfChange() {
    this.loginData.cpf = this.cpf.replace(/[^0-9]/g, '');
    this.cpf = this.loginData.cpf
      .replace(/^(...)(.)/, '\$1\.\$2')
      .replace(/^(.......)(.)/, '\$1\.\$2')
      .replace(/^(.{11,11})(.)/, '\$1\-\$2')
      ;
  }

  private recuperarContaSucesso(data, copia): void {
    this.emails = this.preencheEmail(data, copia);
    this.modalConta.show();
  }

  private recuperarSenhaSucesso(data, copia): void {
    this.emails = this.preencheEmail(data, copia);
    this.modalSenha.show();
  }

  get staticAbas() {
    return EsquecisenhaComponent.ABAS;
  }
}
