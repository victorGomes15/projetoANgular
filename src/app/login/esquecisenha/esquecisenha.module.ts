import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EsquecisenhaRoutingModule } from './esquecisenha-routing-module';
import { EsquecisenhaComponent } from './esquecisenha.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { AlertModule } from '../../components/alert/alert.module';

@NgModule({
  imports: [
    CommonModule,
    EsquecisenhaRoutingModule,
    ModalModule.forRoot(),
    FormsModule, // Ativa as Tags Angular no HTML
    AlertModule // Ativa as mensagens de alertas
  ],
  declarations: [
    EsquecisenhaComponent
  ],
  exports: [
    EsquecisenhaComponent
  ]
})
export class EsquecisenhaModule { }
