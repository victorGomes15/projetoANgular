import { Injectable } from '@angular/core';
import { HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/';
import { AlertService } from '../../components/alert/_services';
import { ConnectContService } from '../../components/connectcont/connectcont.services';
import { ErrorHandlerService } from '../../components/util/errorhandlers/error-handler-service';
import { ResourceNotFoundException } from '../../components/util/errorhandlers/_model/error-handler-models';

@Injectable()
export class EsquecisenhaService {

  constructor(
    private _alertService: AlertService,
    private _connectContService: ConnectContService,
    private _errorHandlerService: ErrorHandlerService
  ) { }

  recuperarConta(loginData): Observable<any> {
    const params: HttpParams = new HttpParams({ fromObject: loginData });
    return this._connectContService.enviaLoginEmail(params)
      .catch((err: any) => this.recuperarErro(err))
      .map(data => data.addresses.map(addresses => addresses.address));
  }

  recuperarSenha(loginData): Observable<any> {
    const params: HttpParams = new HttpParams({ fromObject: loginData });
    return this._connectContService.trocaSenha(params)
      .catch((err: any) => this.recuperarErro(err))
      .map(data => data.addresses.map(addresses => addresses.address));
  }

  recuperarErro(err): any {
    this._errorHandlerService.handleError(err, {
      customHandlers: new Map().set( 404, {
        getError(error: HttpErrorResponse): Error {
          return new ResourceNotFoundException(error.error.error || 'O usuário não foi encontrado.');
        }
      } )
    } );
    return err;
  }

}
