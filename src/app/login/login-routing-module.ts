//https://angular.io/guide/router#refactor-the-routing-configuration-into-a-routing-module
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlankLayoutComponent } from "../components/common/layouts/blankLayout.component";
import { LoginComponent } from './login/login.component';
import { AlterarsenhaComponent } from './alterasenha/alterarsenha.component';

const loginRoutes: Routes = [
    { path: 'siteAlterarsenha', component: AlterarsenhaComponent },
    { path: 'login', component: LoginComponent },
    { path: 'login/:id', component: LoginComponent }
];
@NgModule({
    imports: [
        RouterModule.forChild(
            loginRoutes
        )
    ],
    exports: [
        RouterModule
    ]
})
export class LoginRoutingModule {}