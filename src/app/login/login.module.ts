import { CommonModule } from '@angular/common';
import { NgModule, OnDestroy, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AlertModule } from '../components/alert/alert.module';
import { LoginRoutingModule } from './login-routing-module';
import { LoginComponent } from './login/login.component';
import { EsquecisenhaModule } from './esquecisenha/esquecisenha.module';
import { AlterarsenhaComponent } from './alterasenha/alterarsenha.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, // Ativa as Tags Angular no HTML
    BrowserModule,
    LoginRoutingModule,
    AlertModule, // Ativa as mensagens de alertas
    EsquecisenhaModule,
  ],
  declarations: [
    LoginComponent,
    AlterarsenhaComponent,
  ],
  exports: [
    LoginComponent,
    EsquecisenhaModule,
    AlterarsenhaComponent,
  ],
  providers: []
})
export class LoginModule implements OnDestroy, OnInit  {

  public nav: any;

  public constructor() {
    this.nav = document.querySelector('nav.navbar');
  }

  public ngOnInit(): any {
    this.nav.className += 'white-bg';
  }

  public ngOnDestroy(): any {
    this.nav.classList.remove('white-bg');
  }

}
