import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/';
import { AlertService } from '../../components/alert/_services';
import { ConnectContService } from '../../components/connectcont/connectcont.services';
import { ErrorHandlerService } from '../../components/util/errorhandlers/error-handler-service';

@Injectable()
export class AlterarsenhaService {

  constructor(
    private _alertService: AlertService,
    private _connectContService: ConnectContService,
    private _errorHandlerService: ErrorHandlerService
  ) { }

  alterarSenha  (loginData: { 'senhaAtual': string, 'senhaNova': string }): Observable<any> {
    const observable = Observable.create(observer => {
      this._connectContService.alterarSenha(loginData).subscribe(
        data  => {
          this._alertService.addSuccess('Senha alterada com sucesso!', undefined, { keepAfterRouteChange: true });
          observer.next(data);
        },
        err => {
          this._alertService.error(this._errorHandlerService.getErrorClass(err).message);
        },
        () => observer.complete()
      );
    });
    return observable;
  }

}
