import { Location } from '@angular/common';
import { Component, OnInit, ViewChild, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from '../../components/alert/_services';
import { ResourceBadRequestExceptionHandler } from '../../components/util/errorhandlers/_handlers/resource-bad-request-exception-handler';
import { AlterarsenhaService } from './alterarsenha.service';
import { ValidationService } from '../../components/common/_service/validation.service';
import { LoginService } from '../../login/login.service';
import { ConnectContService } from '../../components/connectcont/connectcont.services';
import 'jquery.complexify';
import { ValidationSelector, Mensagens } from '../_model/validation.model';

declare var jQuery: any;

@Component({
  moduleId: module.id, // Obrigatorio se for trabalhar com templateUrl ou para referenciar qualquer arquivo externo
  selector: 'app-alterarsenha',
  templateUrl: './alterarsenha.component.html',
  styleUrls: ['./alterarsenha.component.scss'],
  providers: [AlterarsenhaService],
  encapsulation: ViewEncapsulation.None,
})
export class AlterarsenhaComponent implements OnInit, AfterViewInit {

  private static readonly MENSAGEM_CONFIRMACAO
     = 'Ao clicar em continuar sua senha será alterada e você será deslogado do sistema. Deseja continuar mesmo assim ?';

  public emails = '';
  public complexity;

  public loginData = {
    senhaAtual: '',
    senhaNova: '',
    senhaConfirmar: '',
  };

  validations: ValidationSelector = new ValidationSelector();

  @ViewChild('myForm')
  private myForm: NgForm;

  constructor(
    public validationService: ValidationService,
    public loginService: LoginService,
    private connectContService: ConnectContService,
    private _service: AlterarsenhaService,
    private alert: AlertService,
    private router: Router,
    private location: Location
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.validationService.addValidCallbackByParameters(
      this.validations, this.myForm,
      { components: 'senhaAtual', condicoes: 'required' }
    );
    this.validationService.addValidCallbackByParameters(
      this.validations, this.myForm,
      { components: 'senhaNova', condicoes: 'required' }
    );
    this.validationService.addValidCallbackByParameters(
      this.validations, this.myForm,
      { components: 'senhaConfirmar', condicoes: 'required' }
    );
    let origin = this.validationService.validationFor(this.validations, { components: 'confirmacao', condicoes: 'minlength' });
    this.validationService.addValidCallback(
      origin, () => !(
        this.myForm.controls['senhaNova'] &&
        this.myForm.controls['senhaNova'].errors &&
        this.myForm.controls['senhaNova'].errors['minlength']
        ||
        this.myForm.controls['senhaConfirmar'] &&
        this.myForm.controls['senhaConfirmar'].errors &&
        this.myForm.controls['senhaConfirmar'].errors['minlength']
      )
    );
    origin = this.validationService.validationFor(this.validations, { components: 'confirmacao', condicoes: 'confirmacaoSenha' });
    this.validationService.addValidCallback(
      origin, () => !(
        this.myForm.controls['senhaNova'] &&
        this.myForm.controls['senhaConfirmar'] &&
        this.myForm.controls['senhaNova'].value !== this.myForm.controls['senhaConfirmar'].value
      )
    );
    this.validationService.addFrom(this.validations, Mensagens.TELAS.TELA_ALTERARSENHA);
    jQuery('#senhaNova').complexify({}, (valid, _complexity) => {
      this.complexity = Math.floor(_complexity);
    });
  }

  public alterarSenha(): void {
    if (this.validationService.isValid(this.validations)) {
      if (confirm(AlterarsenhaComponent.MENSAGEM_CONFIRMACAO)) {
        this._service.alterarSenha({
          'senhaAtual': this.loginData.senhaAtual,
          'senhaNova': this.loginData.senhaNova,
        }).switchMap(next => this.connectContService.getUsuario()).subscribe(next => {
          this.loginService.loginData.username = next.usuarioId;
          this.router.navigate(['/login'], { queryParamsHandling: 'merge'});
        });
      }
    } else {
      this.validationService.showMessages(this.validations);
    }
  }

  public voltar() {
    this.location.back();
  }

}
