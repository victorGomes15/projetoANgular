import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterarsenhaComponent } from './alterarsenha.component';
import { AlterarsenhaService } from './alterarsenha.service';
import { Observable } from 'rxjs/Rx';

describe('AlterarsenhaComponent', () => {
  let component: AlterarsenhaComponent;
  let fixture: ComponentFixture<AlterarsenhaComponent>;
  let service: AlterarsenhaService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterarsenhaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterarsenhaComponent);
    component = fixture.componentInstance;
    service = fixture.debugElement.injector.get(AlterarsenhaService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
