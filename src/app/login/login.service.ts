import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'app/components/util/rxjs-extensions';
import { Cookie } from 'ng2-cookies';
import { AlertService } from '../components/alert/_services/index';
import * as oauth2 from '../security/oauth2/oauth2.constants';
import { OAuth2Service } from '../security/oauth2/oauth2.service';
import { ErrorHandlerService } from '../components/util/errorhandlers/error-handler-service';

@Injectable()
export class LoginService {

  public loginData = { username: '', password: '' };

  constructor(
    private _router: Router,
    private _alertService: AlertService,
    private _errorHandlerService: ErrorHandlerService,
    private _oAuth2Service: OAuth2Service,
  ) { }

  resetLoginData() {
    this.loginData = { username: '', password: '' };
  }

   setParams(loginData): HttpParams {
    const params = this._oAuth2Service.getTokenParams()
    .set('username', loginData.username)
    .set('password', loginData.password)
    .set('grant_type', oauth2.grantTypePassword);
    return params;
  }

  obtainAccessToken(loginData?) {
    const params = this.setParams(loginData || this.loginData);
    this._oAuth2Service.obtainToken(params).subscribe(
      data  => { this.redirect(); this.saveCookieSessionLogin(params); this.resetLoginData(); },
      err   => this._alertService.error('Login ou Senha inválido')
    );
  }

  redirect() {
    this._router.navigate(['/home/usuarios'], { queryParamsHandling: 'merge'});
  }

  saveCookieSessionLogin(params) {
    Cookie.set('session_login', params.get('username'));
    localStorage.removeItem('usuario');
  }

  checkCredentials() {
    if (!Cookie.check(oauth2.accessToken)) {
      if (!Cookie.check(oauth2.refreshToken)) {
        this._router.navigate(['/', { queryParamsHandling: 'merge'}]);
      }
    }
  }

  logout() {
    Cookie.delete(oauth2.accessToken);
    Cookie.delete(oauth2.refreshToken);
    this._router.navigate(['/', { queryParamsHandling: 'merge'}]);
  }
}
