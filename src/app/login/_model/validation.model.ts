import { Observable } from 'rxjs/';

export class ValidationSelector {
    validations
        : ValidationSelector[]  = [];
    conditionForValidation
        : any;
    mensagem
        = '';
    eventForValidation
        : any = 'all';
    componentForValidation
        : string;
    valid
        = true;
    validCallback
        : () => boolean = (() => true);
}

export class Mensagens {

    // TODO: TEMPLATE
    static readonly TEMPLATE_CAMPO_OBRIGATORIO = 'O campo ${campo} é obrigatório';

    public static readonly TELAS = {
        TELA_ALTERARSENHA: [
            {
                components  : 'confirmacao',
                condicoes   : 'confirmacaoSenha',
                mensagem    : 'As senhas não estão iguais.'
            },
            {
                components  : 'confirmacao',
                condicoes   : 'minlength',
                mensagem    : 'Por favor insira no mínimo 8 caracteres.'
            },
        ],
        TELA_ESQUECI: [
            {
                components  : 'username',
                condicoes   : 'required',
                mensagem    : 'Por favor preencher o login.'
            },
            {
                components  : 'codigo',
                condicoes   : 'required',
                mensagem    : 'Por favor, preencha o Código/Apelido de Acesso.'
            },
            {
                components  : 'cpf',
                condicoes   : 'required',
                mensagem    : 'Por favor, preencha o CPF.'
            },
        ],
        TELA_RODAPE: [
            {
                components  : 'footer.titulo',
                condicoes   : 'required',
                mensagem    : 'A descrição é obrigatório.'
            },
        ],
        TELA_CONTATO: [
            {
                components  : 'contatoEmailDestinatario',
                condicoes   : 'email',
                mensagem    : 'E-mail é inválido.'
            },
        ],
        TELA_CABECALHO: [
            {
                components  : 'headerHasEmailValue',
                condicoes   : 'email',
                mensagem    : 'E-mail é inválido.'
            },
            {
                components  : 'hasPhone',
                condicoes   : 'pattern',
                mensagem    : 'Telefone fora do padrão.'
            },
        ]
    };
}
