import { HttpParams } from '@angular/common/http';

export class ParametrosComuns {
    getAllAsHttpParams(): HttpParams {
        let params = new HttpParams();
        for (const [key, value] of Object.entries(this)) {
            if (this.isEmpty(value)) {
                params = params.append(key, value.toString());
            }
        }
        return params;
    }
    isEmpty(value) {
        return value !== undefined;
    }
}

export class ParametrosPesquisaUsuarioContadorOnline extends ParametrosComuns {
    public situacao: string;
    public bloqueado: boolean;
    public filtraContadorCliente: boolean;
    public projectionTemplate: string;
    public permissaoTemplate: string;
    public removeUsuarioLogado: boolean;
    public clienteId: number;
    public departamentoId: number;
    public usuarioNot: number[];

    public size = 50;

    public static createParamsDefault(): ParametrosPesquisaUsuarioContadorOnline {
        const params = new ParametrosPesquisaUsuarioContadorOnline();
        params.projectionTemplate = 'ID_NOME_PROJECTION';
        params.removeUsuarioLogado = true;
        return params;
    }

    public static createParamsRemetente(): ParametrosPesquisaUsuarioContadorOnline {
        const response = ParametrosPesquisaUsuarioContadorOnline.createParamsDefault();
        response.situacao = 'ATIVO';
        response.filtraContadorCliente = true;
        return response;
    }

    public static createParamsDestinatario(): ParametrosPesquisaUsuarioContadorOnline {
        const response = ParametrosPesquisaUsuarioContadorOnline.createParamsDefault();
        response.situacao = 'ATIVO';
        response.bloqueado = false;
        response.permissaoTemplate = 'MENSAGENS_RECEBIDAS';
        response.removeUsuarioLogado = false;
        return response;
    }

}

export class ParametrosPesquisaEmpresa extends ParametrosComuns {
    public situacao = 'ATIVO';
    public situacaoGestor = 'ATIVO';
    public size = 50;
    public projectionTemplate = 'EMPRESAS_COMBO_ORDER_RAZAO';
    public sincronizar; // INFORMAR FOLHA ou CONTABIL para que sejam carregadas apenas as empresas configuradas como true
    public primeiroAcesso;
    public pesquisar;
    public cpfcnpj;
    public dataDesativacaoApos;
    public desativadaAte;
}

