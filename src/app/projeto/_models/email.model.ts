export class Email {
    nomeDestinatario: string;
    emailDestinatario: string;
    emailRemetente: string;
    nome: string;
    email: string;
    mensagem: string;
}

export class EmailType {
    remetente: string;
    assunto: string;
    template: string;
    mensagemTemplates: MensagemTemplate[];
    listaTemplates: ListaTemplate[];
    destinatarios: Destinatario[];
    configuracao: ConfiguracaoEmail;
    anexos: Anexo[];
}

export class ListaTemplate {
    constructor(public campo: string, public valores: MensagemTemplate[]) { }
}

export class MensagemTemplate {
    constructor(public campo: string, public valor: string) { }
}

export class Destinatario {
    constructor(public endereco: string) { }
}

export class ConfiguracaoEmail {
    constructor(public formatacao: string, public quantidadeThreads: number, public prioridade: string) { }
}

export class Anexo {
    constructor(public nome: string, public tamanhoAnexo: number) { }
}
