export class Page<T> {
    content: T[];
    totalElements: number;
}

export class ItemDescricaoValor {
    descricao: string;
    valor: number;
}

export interface Referenciavel {
    readonly referencia: string;
}

export interface OpcaoMultipla<Tipo extends Referenciavel> extends Referenciavel {
    readonly itens: Tipo[];
}

export class OpcoesAtivoInativo extends ItemDescricaoValor implements Referenciavel {

    static readonly ATIVO   = new OpcoesAtivoInativo({ descricao: 'Ativo'     , valor: 1 });
    static readonly INATIVO = new OpcoesAtivoInativo({ descricao: 'Inativo'   , valor: 0 });
    static readonly ITENS = [
        OpcoesAtivoInativo.ATIVO,
        OpcoesAtivoInativo.INATIVO
    ];

    readonly referencia: string;

    constructor(
        item: ItemDescricaoValor,
    ) {
        super();
        Object.assign(this, item);
        this.referencia = item.descricao;
    }
}

export class OpcoesSimNao extends ItemDescricaoValor implements Referenciavel {

    static readonly SIM = new OpcoesSimNao({ descricao: 'Sim'   , valor: 1 });
    static readonly NAO = new OpcoesSimNao({ descricao: 'Não'   , valor: 0 });
    static readonly ITENS = [
        OpcoesSimNao.SIM,
        OpcoesSimNao.NAO
    ];

    readonly referencia: string;

    constructor(
        item: ItemDescricaoValor,
    ) {
        super();
        Object.assign(this, item);
        this.referencia = item.descricao;
    }
}

export class OpcoesComTodos<Tipo extends Referenciavel> {
    readonly mapOpcoes: Map<string, OpcaoMultipla<Tipo>>;
    readonly todos: OpcaoMultipla<Tipo>;

    constructor(
        itens: Tipo[],
        descricaoTodos = 'Todos'
    ) {
        const todasOps = itens.map( item => {
            const clone = Object.assign({}, item, { itens: <Tipo[]>[] });
            clone.itens = [ clone ];
            return <OpcaoMultipla<Tipo>>clone;
        } );
        this.todos = <OpcaoMultipla<Tipo>>{ referencia: descricaoTodos, itens: itens };
        let ops = [ this.todos ];
        ops = ops.concat(todasOps);
        this.mapOpcoes = new Map<string, any>();
        ops.forEach( item =>  this.mapOpcoes.set(item.referencia, item) );
    }

    getValueFromReferencia(item: string): Tipo[] {
        return this.mapOpcoes.get(item).itens;
    }

    getValue(item: Tipo): Tipo[] {
        return this.getValueFromReferencia(item.referencia);
    }

    getValuesOpcoesComTodos(): OpcaoMultipla<Tipo>[] {
        return Array.from(this.mapOpcoes.values());
    }
}
