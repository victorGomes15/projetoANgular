
export class ContadorResource {
    public clienteId: number;
    public pessoaJuridica: PessoaJuridicaResource = new PessoaJuridicaResource();
    public responsavelFinanceiro: ResponsavelFinanceiroResource = new ResponsavelFinanceiroResource();
    public produtos: ProdutoResource[] = [];
    public site: string;
    public situacao: string;
    public aliasSite: string;
    public apelido: string;
    public tamanhoLimite: number;
    public quantidadeCliente: number;
    public inadimplente: number;
    public desistente: boolean;
    public senha1: string;
    public senha2: string;
    public telefones: TelefoneResource[] = [];
    public responsavel: string;

    constructor() { }
}

export class PessoaJuridicaResource {
    public nomeFantasia: string;
    public razaoSocial: string;
    public cnpj: string;
    public emails: EmailResource[] = [];
    public telefones: TelefoneResource[] = [];
}

export class ResponsavelFinanceiroResource {
    public nome: string;
    public cpf: string;
    public emails: EmailResource[] = [];
    public telefones: TelefoneResource[] = [];
}

export class TelefoneResource {
    public ddi: string;
    public ddd: string;
    public numero: string;
    public ramal: string;
    public tipo: string;
}

export class ProdutoResource {
    public produtoId: string;
    public nome: string;
    public situacao: string;
    public responsavel: string;
    public email: string;
}

export class EmailResource {
    public endereco: string;
    public tipo: string;
}
