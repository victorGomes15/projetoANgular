import { SafeHtml } from "@angular/platform-browser";

export class DepartamentoResource {
    private static instance: DepartamentoResource;
    public id: number;
    public descricao: string;

    public static default() {
        if (!this.instance) {
            this.instance = new DepartamentoResource();
            this.instance.id = undefined;
            this.instance.descricao = 'Todos os departamentos';
        }
        return this.instance;
    }
}

export class ClienteContadorOnlineResource {
    public campoPesquisa: string;

    public id: number;
    public cnpjCpfCei: string;
    public email: string;
    public fax: string;
    public razaoSocial: string;
    public apelido: string;
    public site: string;
    public situacao: string;
    public telefone: string;
    public dataImportacao: Date;
    public situacaoImportacao: string;
    public contadorId: number;
    public nomeResponsavel: string;
    public cpfResponsavel: string;
    public emailResponsavel: string;
    public tipoIRPJ: TipoIRPJContadorOnlineResource = new TipoIRPJContadorOnlineResource();
    public situacaoGestor: string;
    public situacaoImportacaoGestor: string;
    public dataDesativacao: Date;
    public dataAlteracao: Date;
    public numeroInicioAta: number;
    public flagNumeroAtaInformada: number;
    public flagNumeroAtaUsuario: string;
    public primeiroAcessoConfirmado: boolean;
    public sincronizarFolhaWeb: boolean;
    public sincronizarContabilWeb: boolean;
    public enviarEmailUnificado: boolean;
    public horaExecucao: Date;
    public enviarEmailGeral: boolean;
    public enabledContadorOnline: boolean;
}

export class UsuarioContadorOnlineResource {
    public id: number;
    public nome: string;
    public email: string;
    public situacao: string;
    public cliente: ClienteContadorOnlineResource = new ClienteContadorOnlineResource();
}

export class TipoIRPJContadorOnlineResource {
    public id: number;
    public descricao: string;
}

export class SearchData {
    public campo: string;
    public id: number;
}

export enum ModalEventType {
    ERRO,
    SUCESSO
}
export class ModalEvent {
    public mensagem: string;
    constructor(public tipo: ModalEventType, public idMensagem, public error?: any) { }
}
