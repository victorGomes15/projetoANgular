import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-correcao',
  templateUrl: './correcao.component.html',
  styleUrls: ['./correcao.component.css']
})
export class CorrecaoComponent implements OnInit {

  anos: Array<number> = new Array();
  anoSelecionado: number = 2019;
  semestre: number = 1;
  semestres: Array<number> = [this.semestre, 2];
  videoUrl;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.anos.push(this.anoSelecionado, 2018, 2017, 2016);
    setTimeout(() => {
      this.mudarVideo();
    }, 3);
  }

  mudarAno(ano: number) {
    this.anoSelecionado = ano;
    setTimeout(() => {
      this.mudarVideo();
    }, 3);

  }

  mudarSemestre(semestre: number) {
    this.semestre = semestre;
    this.mudarVideo();
  }

  mudarVideo() {
    if (this.anoSelecionado == 2019) {
      if (1 == this.semestre) {
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/iHhcHTlGtRs');
      }
      if (2 == this.semestre) {
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/vGcX3adm-IE');
      }
    }
    if (this.anoSelecionado == 2018) {
      if (1 == this.semestre) {
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/F4mhhmyms8s?ecver=1&amp;autoplay=1');
      }
      if (2 == this.semestre) {
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/D5Nk9CECEs0');
      }
    }
    if (this.anoSelecionado == 2017) {
      if (1 == this.semestre) {
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/xAJRdUixqeM');
      }
      if (2 == this.semestre) {
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/xY0VUWZDhMM');
      }
    }
    if (this.anoSelecionado == 2016) {
      if (1 == this.semestre) {
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/LAhYmtEhOw8');
      }
      if (2 == this.semestre) {
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/jOy17euoeUk');
      }
    }

  }

}
