import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { IboxtoolsModule } from '../../components/common/iboxtools/iboxtools.module';
import { LayoutsModule } from '../../components/common/layouts/layouts.module';
import { TitleModule } from '../../components/common/title/title.module';
import { DataTableModule } from '../../components/data-table/data-table.module';
import { DataModule } from '../../components/util/date/data.module';
import { AlertModule } from '../../components/alert/alert.module';
import { ModalModule } from 'ngx-bootstrap';
import { UsuariosRoutingModule } from './usuario-routing.module';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { EmbedVideo } from 'ngx-embed-video';
import { UsuariosComponent } from './usuarios.component';
import { UsuarioService } from './_service/usuarios.service';

@NgModule({
    imports: [
        CommonModule,
        // LayoutsModule,
        FormsModule,
        UsuariosRoutingModule,
        TitleModule,
        DataTableModule,
        IboxtoolsModule,
        DataModule,
        AlertModule,
        Ng2GoogleChartsModule,
        ModalModule.forRoot(),
        EmbedVideo.forRoot()
    ],
    declarations: [
        UsuariosComponent,
    ],
    providers: [
        UsuarioService
    ]
})
export class UsuarioModule { }
