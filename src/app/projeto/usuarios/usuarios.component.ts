import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { UsuarioService } from './_service/usuarios.service';
import { GoogleChartInterface, GoogleChartComponentInterface } from 'ng2-google-charts/google-charts-interfaces';
import { GoogleChartComponent } from 'ng2-google-charts';
import { EmbedVideoService } from 'ngx-embed-video';
import { SafeHtml } from '@angular/platform-browser';
import { SessionService } from '../../components/common/_service/session.service';
import { AlertService } from '../../components/alert/_services';

@Component({
  selector: 'app-usuarios.component',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit, AfterViewInit {

  @ViewChild('chart')
  chartcomponent: GoogleChartInterface;

  public tableChart = {
    chartType: 'Bar',
    dataTable: [
      ['ano/semestre', 'Sua Nota', 'Média Turma', 'Média Curso']
    ],
    formatters: [
      {
        columns: [1, 2, 3],
        type: 'NumberFormat',
        options: {
          negativeColor: 'red', negativeParens: true
        }
      }
    ],
    options: { title: 'Notas', allowHtml: true }
  };

  //////////////////////////////////////////////////////////////
  ///////////////////////// CONSTRUTOR /////////////////////////
  //////////////////////////////////////////////////////////////

  constructor(
    private usuariosService: UsuarioService,
    private sessionService: SessionService,
    public alertService: AlertService,
    private modalRef: BsModalService,
  ) { }

  //////////////////////////////////////////////////////////////
  /////////////////////// INICIALIZACAO ////////////////////////
  //////////////////////////////////////////////////////////////

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.usuariosService.getNotas().subscribe(data => {
      data.forEach(nota => {
        let notaArray: Array<any> = [];
        notaArray.push(nota.anoSemestre, nota.notaAluno, nota.mediaTurma, nota.mediaCurso);
        this.tableChart.dataTable.push(notaArray);
        // this.chartcomponent.component.draw();
      });
    }, error => { console.log("ERROW", error) })
  }

}
