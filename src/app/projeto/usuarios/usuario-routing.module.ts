import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from '../../security/guards/permission.guard';
import { UsuariosComponent } from './usuarios.component';

const routes: Routes = [
    {
        path: 'home/usuarios', component: UsuariosComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsuariosRoutingModule {

}
