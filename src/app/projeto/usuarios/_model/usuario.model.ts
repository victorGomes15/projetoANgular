export class Nota {
    public anoSemestre: string;
    public notaAluno: number;
    public mediaTurma: number;
    public mediaCurso: number;
}
