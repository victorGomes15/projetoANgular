import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Nota } from '../_model/usuario.model';
import { environment } from '../../../../environments/environment';
import { HeaderUtils } from '../../../components/util/headers/header-utils';

@Injectable()
export class UsuarioService {

    private readonly urlGetUsuarios: string = environment.urlSSO + '/notas';

    constructor(private http: HttpClient) { }

    public getNotas() {
        return this.http.get<Array<Nota>>(this.urlGetUsuarios, {
            headers: HeaderUtils.getHeaderWithAuthorization()
        });
    }

}
