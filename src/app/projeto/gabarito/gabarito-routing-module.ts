// https://angular.io/guide/router#refactor-the-routing-configuration-into-a-routing-module
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GabaritoComponent } from './gabarito.component';
import { AlertService } from '../../components/alert/_services';

const homeRoutes: Routes = [
    { path: 'home', component: GabaritoComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(
            homeRoutes
        )
    ],
    providers: [
        AlertService
    ],
    exports: [
        RouterModule
    ]
})

export class GabaritoRoutingModule {}
