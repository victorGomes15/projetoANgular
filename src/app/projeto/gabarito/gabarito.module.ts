import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';
import { AlertModule } from '../../components/alert/alert.module';
import { GabaritoRoutingModule } from './gabarito-routing-module';
import { GabaritoComponent } from './gabarito.component';
import { LayoutsModule } from '../../components/common/layouts/layouts.module';
import { TitleModule } from '../../components/common/title/title.module';
import { NgxMaskModule } from 'ngx-mask';
import { DataTableModule } from '../../components/data-table/data-table.module';
import { IboxtoolsModule } from '../../components/common/iboxtools/iboxtools.module';
import { DataModule } from '../../components/util/date/data.module';
import { GabaritoService } from './_service/gabarito.service';
import { PdfViewerModule } from 'ng2-pdf-viewer';
@NgModule({
  imports: [
    CommonModule,
    FormsModule, // Ativa as Tags Angular no HTML
    BrowserModule,
    GabaritoRoutingModule,
    AlertModule, // Ativa as mensagens de alertas
    NgxPaginationModule,
    TitleModule,
    BrowserAnimationsModule,
    // LayoutsModule,
    IboxtoolsModule,
    // ContadorOnlineModule,
    NgxMaskModule.forRoot(),
    DataTableModule,
    DataModule,
    AlertModule,
    PdfViewerModule
  ],
  declarations: [
    GabaritoComponent
  ],
  exports: [
    GabaritoComponent
  ],
  providers: [GabaritoService]
})
export class GabaritoModule { }
