import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HeaderUtils } from '../../../components/util/headers/header-utils';

@Injectable()
export class GabaritoService {

    private readonly urlGetAutorizacao: string = environment.urlSSO + '/v1/autorizacoes';

    constructor(private http: HttpClient) {
    }

    public getAutorizacao(page: string, size: string) {
        const params: HttpParams = new HttpParams();
        params.append('page', page); 
        params.append('size', size);
        return this.http.get<any>(this.urlGetAutorizacao, {
            headers: HeaderUtils.getHeaderWithAuthorization(), params: params, observe:'response'
        });
    }
}
