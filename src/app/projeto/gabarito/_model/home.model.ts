import { FiltroPage } from "../../../components/data-table/_models/paginacao.model";
import { ClienteContadorOnlineResource } from "../../_models/contadoronline";
import { DataTable } from "../../../components/data-table/_models/data-table.model";

export class AutorizacaoTable extends DataTable {
    public id: any;
    public contadorId: any;
    public createdBy: any;
    public dataCriacao: any;
}

export class AutorizacaoResource {
    public id: any;
    public contadorId: any;
    public createdBy: any;
    public dataCriacao: any;
}