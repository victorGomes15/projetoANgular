import { Component, OnInit } from '@angular/core';
import { DataTableColumn, DataTableConfig, PaginacaoConfig, PaginacaoAlignment, DataTable } from '../../components/data-table/_models/data-table.model';
import { SessionService } from '../../components/common/_service/session.service';
import { ButtonGroup } from '../../components/data-table/_models/button.model';
import { DatePipe } from '@angular/common';
import { AutorizacaoTable, AutorizacaoResource } from './_model/home.model';
import { GabaritoService } from './_service/gabarito.service';
import { HeaderUtils } from '../../components/util/headers/header-utils';
import { Pageable } from '../../components/data-table/_models/paginacao.model';

declare var jQuery: any;

@Component({
  selector: 'app-home',
  templateUrl: 'gabarito.component.html',
  styleUrls: ['./gabarito.component.scss', './main.css']
})
export class GabaritoComponent implements OnInit {

  pdfSrc: string = '/assets/Gabarito-TP-Noite.pdf';
  downloadFile = '/assets/prova.pdf';
  anos: Array<number> = new Array();
  anoSelecionado: number = 2019;
  semestre: number = 1;
  semestres: Array<number> = [this.semestre, 2];
  prova: string = 'A'
  tipoProvas: Array<string> = [this.prova, 'B', 'C', 'D'];

  //////////////////////////////////////////////////////////////
  ///////////////////////// CONSTRUTOR /////////////////////////
  //////////////////////////////////////////////////////////////

  constructor(
    private sessionService: SessionService,
    private homeService: GabaritoService
  ) { }

  //////////////////////////////////////////////////////////////
  /////////////////////// INICIALIZACAO ////////////////////////
  //////////////////////////////////////////////////////////////

  ngOnInit() {
    this.anos.push(this.anoSelecionado, 2018, 2017, 2016);
  }

  mudarAno(ano: number) {
    this.anoSelecionado = ano;
    this.mudarGabarito();
  }

  mudarSemestre(semestre: number) {
    this.semestre = semestre;
    this.mudarGabarito();
  }

  mudarProva(prova: string) {
    this.prova = prova;
    this.mudarGabarito();
  }

  downloadProva() {
    let link = document.createElement('a');
    link.setAttribute('type', 'hidden');
    link.href = this.downloadFile;
    link.download = this.downloadFile;
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  mudarGabarito() {
    if (this.anoSelecionado == 2019) {
      if (1 == this.semestre) {
        if (this.prova == 'A') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }
        if (this.prova == 'B') {
          this.pdfSrc = '/assets/2019-1-b.pdf';
        }

        if (this.prova == 'C') {
          this.pdfSrc = '/assets/2019-1-c.pdf';
        }

        if (this.prova == 'D') {
          this.pdfSrc = '/assets/2019-1-d.pdf';
        }
      }
      if (2 == this.semestre) {
        if (this.prova == 'A') {
          this.pdfSrc = '/assets/2019-2-a.pdf';
        }
        if (this.prova == 'B') {
          this.pdfSrc = '/assets/2019-2-b.pdf';
        }

        if (this.prova == 'C') {
          this.pdfSrc = '/assets/2019-2-c.pdf';
        }

        if (this.prova == 'D') {
          this.pdfSrc = '/assets/2019-2-d.pdf';
        }
      }
    }
    if (this.anoSelecionado == 2018) {
      if (1 == this.semestre) {
        if (this.prova == 'A') {
          this.pdfSrc = '/assets/2018-1-a.pdf';
        }
        if (this.prova == 'B') {
          this.pdfSrc = '/assets/2018-1-b.pdf';
        }

        if (this.prova == 'C') {
          this.pdfSrc = '/assets/2018-1-c.pdf';
        }

        if (this.prova == 'D') {
          this.pdfSrc = '/assets/2018-1-d.pdf';
        }
      }
      if (2 == this.semestre) {
        if (this.prova == 'A') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }
        if (this.prova == 'B') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }

        if (this.prova == 'C') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }

        if (this.prova == 'D') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }
      }
    }
    if (this.anoSelecionado == 2017) {
      if (1 == this.semestre) {
        if (this.prova == 'A') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }
        if (this.prova == 'B') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }

        if (this.prova == 'C') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }

        if (this.prova == 'D') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }
      }
      if (2 == this.semestre) {
        if (this.prova == 'A') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }
        if (this.prova == 'B') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }

        if (this.prova == 'C') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }

        if (this.prova == 'D') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }
      }
    }
    if (this.anoSelecionado == 2016) {
      if (1 == this.semestre) {
        if (this.prova == 'A') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }
        if (this.prova == 'B') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }

        if (this.prova == 'C') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }

        if (this.prova == 'D') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }
      }
      if (2 == this.semestre) {
        if (this.prova == 'A') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }
        if (this.prova == 'B') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }

        if (this.prova == 'C') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }

        if (this.prova == 'D') {
          this.pdfSrc = '/assets/Gabarito-TP-Noite.pdf';
        }
      }
    }

  }


}
