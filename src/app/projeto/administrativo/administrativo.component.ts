import { Component, OnInit, ɵConsole } from '@angular/core';
import { AlertService } from '../../components/alert/_services';
import { ProjetoService } from '../_services/projeto.service';

@Component({
  selector: 'app-administrativo',
  templateUrl: './administrativo.component.html',
  styleUrls: ['./administrativo.component.css', './administrativo.component.scss']
})
export class AdministrativoComponent implements OnInit {

  disableUpload = true;
  disableCorrecao = true;
  disableGab = true;
  anos: Array<number> = [];
  semestres: Array<number> = [1, 2];
  tipoProvas: Array<string> = ['A', 'B', 'C', 'D'];
  private anoSelecionadoCorrecao;
  private anoGabProvaSelecionado;
  private fileCsv: File

  constructor(private alertaService: AlertService, private projetoService: ProjetoService) { }

  ngOnInit() {
    this.anos.push(2019, 2018, 2017, 2016);
  }

  teste($event) {
    const fileSelected: File = $event.target.files[0];
    if (fileSelected.type != 'application/vnd.ms-excel') {
      this.alertaService.error('Tipo de arquivo inválido, Selecione outro');
      this.disableUpload = true;
    } else {
      this.disableUpload = false;
      this.fileCsv = fileSelected;
      console.log('AAA', this.fileCsv);
    }
  }

  mudarAnoCorrecao(ano: number) {
    this.anoSelecionadoCorrecao = ano;
  }

  mudarAnoGabProva(ano: number) {
    this.anoGabProvaSelecionado = ano;
  }

  uploadTp() {
    const fd = new FormData();
    fd.append('multipartFile', this.fileCsv);
    console.log('AAA', this.fileCsv);
    this.projetoService.uploadFile(fd).subscribe(data => {
      console.log('SUCESSO');
    });
  }

}
