import { CommonModule } from '@angular/common';
import { NgModule, OnDestroy, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AlertModule } from '../../components/alert/alert.module';
import { AdministrativoComponent } from './administrativo.component';
import { AlertService } from '../../components/alert/_services';
import { TitleModule } from '../../components/common/title/title.module';


@NgModule({
  imports: [
    CommonModule,
    TitleModule,
    FormsModule, // Ativa as Tags Angular no HTML
    BrowserModule,
    AlertModule, // Ativa as mensagens de alertas
  ],
  declarations: [
    AdministrativoComponent
  ],
  exports: [
    AdministrativoComponent
  ],
  providers: [AlertService]
})
export class AdministrativoModule  {

}
