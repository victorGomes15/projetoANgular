import { Injectable, Sanitizer } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Email, EmailType, Destinatario, MensagemTemplate, ConfiguracaoEmail, Anexo, ListaTemplate } from '../_models/email.model';
import { Cookie } from 'ng2-cookies';
import * as oauth2 from '../../security/oauth2/oauth2.constants';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs/';

@Injectable()
export class EmailService {

    constructor(private http: HttpClient, private sanitizer: DomSanitizer) { }

    readonly url_notify: string = environment.urlSSO + '/emails';
    private readonly url_template: string = environment.urlSSO + '/templateContents';

    readonly access_token: string = 'access_token';
    readonly refresh_token: string = 'refresh_token';
    readonly client_secret: string = 'e9a7b424-f162-4ac2-9493-9407b7a54917';
    readonly client_id: string = '16';
    readonly grant_type: string = 'client_credentials';
    readonly nomeDoAnexo: string = 'chaveDeacesso.txt';

    setParams(): HttpParams {
        const params = new HttpParams()
            .set('grant_type', this.grant_type)
            .set('client_id', this.client_id)
            .set('client_secret', this.client_secret);
        return params;
    }

    public sendEmail(email: Email) {
        const formData = this.createRequest(email);
        this.enviarEmail(Cookie.get(oauth2.accessToken), formData);
    }

    private enviarEmail(data: string, formData: FormData) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', 'Bearer ' + data);
        headers = headers.append('Accept', 'application/json');
        this.http.post(this.url_notify, formData, { headers: headers })
            .subscribe(
                data2 => { console.log('Email enviado!'); console.log(data2); },
                erro => { console.error('Erro ao enviar email', erro); });
    }

    private createFormData(emailType: EmailType) {
        const formData = new FormData();
        const blob = new Blob([JSON.stringify(emailType)], { type: 'application/json' });
        formData.append('resources', blob);
        return formData;
    }

    private createRequest(email: Email) {
        const emailType = new EmailType();
        const anexoFile = this.createAnexoFile(email.mensagem);
        emailType.assunto = 'Chave de Acesso';
        emailType.remetente = email.emailRemetente;
        emailType.template = 'CHAVE_DE_ACESSO_CONTADOR_ONLINE_SITE';
        emailType.destinatarios = this.createEmailDestinatarios(email.emailDestinatario);
        emailType.mensagemTemplates = this.createMensagemTemplate(email.mensagem);
        emailType.configuracao = new ConfiguracaoEmail('text/html', 1, 'ALTA');
        emailType.anexos = this.createAnexo(anexoFile);
        const formData = this.createFormData(emailType);
        formData.append('file', anexoFile, this.nomeDoAnexo);
        return formData;
    }

    private createMensagemTemplate(chave: string) {
        const mensagensTemplate = [];
        mensagensTemplate.push(new MensagemTemplate('chave', chave));
        return mensagensTemplate;
    }

    private createEmailDestinatarios(emailDestinatario: string) {
        const destinatarios = [];
        destinatarios.push(new Destinatario(emailDestinatario));
        return destinatarios;
    }

    private createAnexo(blob: Blob) {
        const anexos = [];
        if (typeof blob !== 'undefined' && blob) {
            anexos.push(new Anexo(this.nomeDoAnexo, blob.size));
        }
        return anexos;
    }

    private createAnexoFile(chaveDeAcesso: string) {
        return new Blob([chaveDeAcesso], { type: 'text/plain' });
    }

    private dadosEmail(dados) {
        const emailType = new EmailType();
        emailType.template = dados.template || '';
        emailType.mensagemTemplates = [
            <MensagemTemplate>{ campo: 'mailContent_url', valor: environment.mailContent_url || '' },
            <MensagemTemplate>{ campo: 'remetente_id', valor: dados.remetente_id },
            <MensagemTemplate>{ campo: 'mailContent_logoPadraoEmail', valor: dados.mailContent_logoPadraoEmail || 'true' },
            <MensagemTemplate>{ campo: 'mailContent_logoConfiguracaoEmail', valor: dados.mailContent_logoConfiguracaoEmail || false },
            <MensagemTemplate>{ campo: 'mailContent_razaoSocial', valor: dados.mailContent_razaoSocial || '' },
        ];
        emailType.listaTemplates = [ <ListaTemplate>{ campo: 'remetente_assinatura', valores: [
            <MensagemTemplate>{ campo: 'ass', valor: dados.ass || '' }
        ] } ];
        return emailType;
    }

    public htmlFromTemplateEmail(dados): Observable<SafeHtml> {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', 'Bearer ' + Cookie.get(oauth2.accessToken));
        headers = headers.append('Accept', 'application/json');
        const emailType = this.dadosEmail(dados);
        return this.http.post<string>(this.url_template, emailType, { headers: headers })
                .map( content => this.sanitizer.bypassSecurityTrustHtml(content) );
    }
}
