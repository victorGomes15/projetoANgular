import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'app/components/util/rxjs-extensions';
import { Cookie } from 'ng2-cookies';
import { Observable } from 'rxjs/';
import { environment } from '../../../environments/environment';
import * as oauth2 from '../../security/oauth2/oauth2.constants';
import { ClienteContadorOnlineResource, DepartamentoResource, UsuarioContadorOnlineResource } from '../_models/contadoronline';
import { ParametrosPesquisaUsuarioContadorOnline, ParametrosPesquisaEmpresa } from '../_models/parametros.model';
import { HeaderUtils } from '../../components/util/headers/header-utils';

@Injectable()
export class ProjetoService {

    readonly servicoDepartamento = `${environment.urlSSO}/departamentos`;
    readonly servicoUplload = `${environment.urlSSO}/api/users`;
    readonly urlDownload = `${environment.urlSSO}/caches/contadoronline/files`;
    SIZE: number;

    constructor(private httpClient: HttpClient) { }

    private getHeaders(): HttpHeaders {
        return new HttpHeaders({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + Cookie.get(oauth2.accessToken),
            'Content-Type': 'multipart/form-data'
        });
    }

    public uploadFile(file): Observable<any> {
        return this.httpClient.post(this.servicoUplload, file,
            { headers: this.getHeaders(), responseType: "arraybuffer" });
    }

    public downloadFile(codigo: string, nome: string): Observable<any> {
        let param = new HttpParams();
        param = param.append('nome', nome);
        return this.httpClient.get(this.urlDownload + '/' + codigo,
            { headers: this.getHeaders(), params: param, responseType: 'blob', observe: 'response' });
    }

}
