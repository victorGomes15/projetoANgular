import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CleanLayoutComponent } from '../components/common/layouts/cleanLayout.component';
import { FullModeGuard } from '../components/guards/full.guard';

const ProjetoRoutes: Routes = [
    // {
    //     path: 'cadastros',
    //     component: CleanLayoutComponent,
    //     loadChildren: './cadastros/cadastros.module#CadastrosModule',
    //     canActivate: [ FullModeGuard ], runGuardsAndResolvers: 'always'
    // },
    // {
    //     path: 'reunioes',
    //     component: CleanLayoutComponent,
    //     loadChildren: './reunioes/reunioes.module#ReuniaoModule',
    //     canActivate: [ FullModeGuard ],
    //     runGuardsAndResolvers: 'always'
    // },
    // {
    //     path: 'mensagens',
    //     component: CleanLayoutComponent,
    //     loadChildren: './mensagem/mensagem.module#MensagemModule',
    //     canActivate: [ FullModeGuard ],
    //     runGuardsAndResolvers: 'always'
    // },
    // {
    //     path: 'processos',
    //     component: CleanLayoutComponent,
    //     loadChildren: './processos/processo.module#ProcessoModule',
    //     canActivate: [ FullModeGuard ],
    //     runGuardsAndResolvers: 'always'
    // },
    // {
    //     path: 'lixeira',
    //     component: CleanLayoutComponent,
    //     loadChildren: './lixeira/lixeira.module#LixeiraModule',
    //     canActivate: [ FullModeGuard ],
    //     runGuardsAndResolvers: 'always'
    // },
    // {
    //     path: 'ferramentas',
    //     component: CleanLayoutComponent,
    //     loadChildren: './ferramentas/ferramentas.module#FerramentasModule',
    //     canActivate: [ FullModeGuard ],
    //     runGuardsAndResolvers: 'always'
    // },
    // {
    //     path: 'site',
    //     component: CleanLayoutComponent,
    //     loadChildren: './site/site.module#SiteModule',
    //     canActivate: [ FullModeGuard ],
    //     runGuardsAndResolvers: 'always'
    // },
    // {
    //     path: 'minhaPagina',
    //     component: CleanLayoutComponent,
    //     loadChildren: './minhaPagina/minhaPagina.module#MinhaPaginaModule',
    //     canActivate: [ FullModeGuard ],
    //     runGuardsAndResolvers: 'always'
    // },
    // {
    //     path: 'arquivos',
    //     component: CleanLayoutComponent,
    //     loadChildren: './arquivos/arquivos.module#ArquivosModule',
    //     canActivate: [ FullModeGuard ],
    //     runGuardsAndResolvers: 'always'
    // }
];

@NgModule({
    imports: [
        RouterModule.forChild(ProjetoRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class ContadorOnlineRoutingModule { }
