import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Ng2CompleterModule } from 'ng2-completer';
import { ScrollToModule } from 'ng2-scroll-to';
import { ModalModule } from 'ngx-bootstrap';
import { ClipboardModule } from 'ngx-clipboard';
import { NgxPaginationModule } from 'ngx-pagination';
import { AlertModule } from '../components/alert/alert.module';
import { TitleModule } from '../components/common/title/title.module';
import { DataTableModule } from '../components/data-table/data-table.module';
import { EmailService } from './_services/email.service';
import { UsuarioModule } from './usuarios/usuario.module';
import { ContadorOnlineRoutingModule } from './projeto-routing.module';
import { GabaritoRoutingModule } from './gabarito/gabarito-routing-module';
import { GabaritoModule } from './gabarito/gabarito.module';
import { ProjetoComponent } from './projeto.component';
import { ProjetoService } from './_services/projeto.service';
import { CorrecaoComponent } from './correcao/correcao.component';
import { AdministrativoComponent } from './administrativo/administrativo.component';
import { AdministrativoModule } from './administrativo/administrativo.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, // Ativa as Tags Angular no HTML
    AlertModule, // Ativa as mensagens de alertas
    NgxPaginationModule,
    ContadorOnlineRoutingModule,
    DataTableModule,
    ClipboardModule,
    ModalModule.forRoot(),
    ScrollToModule.forRoot(),
    TitleModule,
    Ng2CompleterModule,
    GabaritoModule,
    UsuarioModule,
    AdministrativoModule
  ],
  declarations: [
    ProjetoComponent,
    CorrecaoComponent,
    // AdministrativoComponent,
  ],
  exports: [
    ProjetoComponent,
  ],
  entryComponents: [
  ],
  providers: [
    EmailService,
    ProjetoService,
  ]
})
export class ContadorOnlineModule { }
