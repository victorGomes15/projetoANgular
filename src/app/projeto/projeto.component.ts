import { Component, AfterViewInit, OnInit } from '@angular/core';
import { saveAs } from 'file-saver/FileSaver';

import { Router, NavigationStart } from '@angular/router';
import { ConnectContService } from '../components/connectcont/connectcont.services';

declare var jQuery: any;

@Component({
  selector: 'app-contador-online',
  templateUrl: './projeto.component.html',
  styleUrls: ['./projeto.component.scss']
})
export class ProjetoComponent implements AfterViewInit, OnInit {


  hiddenAdministrativo = true;

  constructor(private router: Router, private connectContService: ConnectContService) { }

  activeRoute(routename: string): boolean {
    return this.router.url.indexOf(routename) > -1;
  }

  ngOnInit(): void {
    this.connectContService.getUsuario().subscribe(
      data => {
        // const roles: Array<any> = data.roles;
        // const isAdmin = roles.filter(role => role.name == 'ROLE_ADMIN');
        // console.log(isAdmin);
        // if (isAdmin.length > 0) {
        //   this.hiddenAdministrativo = false;
        // } else {
        //   this.hiddenAdministrativo = true;
        // }
      }
    );
  }


  ngAfterViewInit() {
    jQuery('#side-menu').metisMenu();

    if (jQuery('body').hasClass('fixed-sidebar')) {
      jQuery('.sidebar-collapse').slimscroll({
        height: '100%'
      });
    }
  }

}
