//https://angular.io/guide/router#redirecting-routes
//https://angular.io/guide/router#refactor-the-routing-configuration-into-a-routing-module
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; //https://angular.io/guide/router#router-imports
import { BasicLayoutComponent } from "./components/common/layouts/basicLayout.component";
import { CleanLayoutComponent } from './components/common/layouts/cleanLayout.component';
import { FullModeGuard } from './components/guards/full.guard';
import { LoginComponent } from './login/login/login.component';
import { AcessoNegadoComponent } from './security/acessoNegado/acessoNegado.component';
import { GabaritoComponent } from './projeto/gabarito/gabarito.component';
import { UsuariosComponent } from './projeto/usuarios/usuarios.component';
import { CorrecaoComponent } from './projeto/correcao/correcao.component';
import { AdministrativoComponent } from './projeto/administrativo/administrativo.component';

const appRoutes: Routes = [
    //Página principal.
    { path: '', component: LoginComponent },
    {
        path: 'home', component: BasicLayoutComponent,
        children: [
            { path: 'autorizados', component: GabaritoComponent },
            { path: 'usuarios', component: UsuariosComponent },
            { path: 'correcoes', component: CorrecaoComponent },
            { path: 'administracoes', component: AdministrativoComponent },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(//https://angular.io/guide/router#configuration
            appRoutes,
            { enableTracing: false } //habilita debug para as rotas no console - https://angular.io/api/router/ExtraOptions
        )
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule { }
