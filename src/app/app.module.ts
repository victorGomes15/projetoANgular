import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login/login.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginModule } from './login/login.module';
import { AlertModule } from './components/alert/alert.module';
import { AcessoNegadoComponent } from './security/acessoNegado/acessoNegado.component';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { OAuth2Service } from './security/oauth2/oauth2.service';
import { OAuth2Interceptor } from './security/oauth2/oauth2.interceptor';
import { LoginService } from './login/login.service';
import { FullModeGuard } from './components/guards/full.guard';
import { ErrorHandlerService } from './components/util/errorhandlers/error-handler-service';
import { AlertService } from './components/alert/_services';
import { ValidationService } from './components/common/_service/validation.service';
import { DataTableCache } from './components/data-table/data-table-cache.component';
import { PermissionGuard } from './security/guards/permission.guard';
import { AcessoNegadoService } from './security/acessoNegado/acessoNegado.service';
import { ConnectContService } from './components/connectcont/connectcont.services';
import { SessionService } from './components/common/_service/session.service';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutsModule } from './components/common/layouts/layouts.module';

@NgModule({
  declarations: [
    AppComponent,
    AcessoNegadoComponent,
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    LayoutsModule,
    LoginModule,
    AlertModule,
    
  ],
  providers: [
    OAuth2Service,
    { provide: HTTP_INTERCEPTORS, useClass: OAuth2Interceptor, multi: true },
    LoginService,
    FullModeGuard,
    ErrorHandlerService,
    AlertService,
    ValidationService,
    DataTableCache,
    PermissionGuard,
    AcessoNegadoService,
    ConnectContService,
    SessionService,],
  bootstrap: [AppComponent]
})
export class AppModule { }
