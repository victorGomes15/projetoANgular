import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';

declare var jQuery: any;
declare var tinyMCE: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent {
  title = 'app';

  constructor(@Inject(APP_BASE_HREF) private href: string) { }

  public ngOnInit(): any {
    window['paceOptions'] = {
      ajax: {
        ignoreURLs: ['sockjs-node'],
        trackWebSockets: false,
        eventLag: false
      }
    };
    window['Pace'].on('start' , () => jQuery('.spinner').show());
    window['Pace'].on('done'  , () => jQuery('.spinner').hide());
    tinyMCE.baseURL = `${this.href}assets/js/tiny_mce`;
  }
}
