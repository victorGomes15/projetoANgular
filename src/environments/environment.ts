// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  clientId: 'cliente',
  clientSecret: '123',
  urlSSO: 'http://localhost:8080/tcc',
  urlSSOPublico: 'http://192.168.5.218:7780/sso-publico',
  urlSite: 'http://site.softmatic.com.br',
  urlPainel: 'http://192.168.5.15:9280/painel',
  mailContent_url: 'http://CONTSDCON01:8080',
  srvImgAcessoNegado: 'http://contsdcon01:8080',
  versions: <{ 'name': string, 'version': string, 'revision': string, 'branch': string, 'date': string }>((() => {
    try {
      return this.require('./git-version.json');
    } catch (e) {
      return { 'name': '-', 'version': '0.0.0', 'revision': '0', 'branch': 'branch', 'date': '00/00/00 00:00:00' };
    }
  })())
};
