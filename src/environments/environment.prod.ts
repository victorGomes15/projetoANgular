export const environment = {
  production: true,
  clientId: '8',
  clientSecret: 'bdbd3b7e-9060-42ac-9e16-72b96caca840',
  urlSSO: 'http://localhost:8087/sso',
  urlSSOPublico: 'http://192.168.5.218:7780/sso-publico',
  urlSite: 'http://site.softmatic.com.br',
  urlPainel: 'http://192.168.5.15:9280/painel',
  mailContent_url: 'http://CONTSDCON01:8080',
  srvImgAcessoNegado: 'http://contsdcon01:8080',
  versions: <{ 'name': string, 'version': string, 'revision': string, 'branch': string, 'date': string }>((() => {
    try {
      return this.require('./git-version.json');
    } catch (e) {
      return { 'name': '-', 'version': '0.0.0', 'revision': '0', 'branch': 'branch', 'date': '00/00/00 00:00:00' };
    }
  })())
};
